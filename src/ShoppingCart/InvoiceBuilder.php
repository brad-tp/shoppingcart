<?php

namespace Owens\ShoppingCart;

use Owens\ShoppingCart\ShoppingCart;
use App\Models\Invoice;
use App\Models\InvoiceItem;

class InvoiceBuilder {
	
	protected $owner;
	protected $invoice;		// public for first testing..
	
	/**
	 * 
	 * @param unknown $owner	-- Owner of the invoice
	 * @param string $invoice	-- Use this existing invoice instead of creating one.
	 */
	function __construct($owner, array $options=[], $invoice=false) {
		$this->owner = $owner;
		$this->invoice = $invoice;
	}
	
	function getInvoiceModel() {
		return config('shoppingcart.models.invoice','Owens\Models\Invoice');
	}
	
	function create($options) {
		$this->invoice = (new \ReflectionClass($this->getInvoiceModel()))->newInstance($options);
	}
	
	function getInvoice() {
		return $this->invoice;
	}
	
	function addItem($item) {
		$invoiceItemParams= $this->buildItemPayload($item);
		$invoiceItem = $this->invoice->addItem($invoiceItemParams,$item->getMeta()->toArray());
		return $invoiceItem;
	}
/*	
	function addItem($item) {
		$invoiceItemParams= $this->buildItemPayload($item);
		$invoiceItem = InvoiceItem::create($invoiceItemParams);
		
		$invoiceItem->setMeta($item->getMeta()->toArray());
		//		$invoiceItem->save();	// Because of adding meta data
		
		$this->invoice->total += $invoiceItem->total;
		$this->invoice->subtotal += $invoiceItem->total;
		return $invoiceItem;
	}
*/	
	
	function buildItemPayload($item) {
		$invoiceItemParams= [
				'buyer_id' => $this->owner->id,
				'seller_id' => $item->getSellerId(),
				'invoice_id' => $this->invoice->id,
		];
		$invoiceItemParams['product_name'] = $item->product_name;
		$invoiceItemParams['description'] = $item->description;
		$invoiceItemParams['qty'] = $item->qty;
		$invoiceItemParams['product_id'] = $item->product_id;
		$invoiceItemParams['product_type'] = ($item->Product->ProductType)?$item->Product->ProductType->name:null;
		$invoiceItemParams['price'] = $item->price;
		$invoiceItemParams['total'] = $invoiceItemParams['qty'] * $invoiceItemParams['price'];
		$invoiceItemParams['seller_id'] = $item->getSellerId();	
		return $invoiceItemParams;
	}
	
	function save() {
		$this->invoice->save();
	}
}
