<?php

namespace Owens\ShoppingCart;

interface ShoppingCartContract {
	public function add(array $params, $meta = []);
	public function remove($id);
	public function total();
	public function empty();
	public function items();
	public function cart();
}