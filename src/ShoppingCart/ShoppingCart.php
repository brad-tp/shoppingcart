<?php
namespace Owens\ShoppingCart;


use \DateTime;
use Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use ReflectionClass;
use Illuminate\Support\Facades\Event;
use Owens\ShoppingCart\Events\ShoppingCartAddEvent;
use Owens\ShoppingCart\Events\ShoppingCartPurcahseItemEvent;
use Owens\ShoppingCart\Exceptions\ConfigurationException;
use Owens\ShoppingCart\InvoiceBuilder;
use Illuminate\Support\Facades\Route;

class ShoppingCart extends ShoppingCartBase {
	protected $cart = false;
	
	// Must define model;
	protected $cart_model = "Owens\ShoppingCart\Models\Cart";
	protected $vendorChargeGroupModel = "Owens\ShoppingCart\Models\VendorChargeGroup";
	protected $itemModel = "Owens\ShoppingCart\Models\CartItem";
	
	protected $cookie_name = "cart";
	protected $member_key = "user_id";
	protected $config;
	protected $orderGroup = false;
	protected $vendorGroup = false;
	
	function init() {
		$this->cart_model = $this->getConfig('models.cart',false,true);
		$this->itemModel = $this->getConfig('models.cart_item',false,true);
		$this->vendorChargeGroupModel = $this->getConfig('models.vendor_charge_group',false,true);
		return $this->getCart();
	}
	
	
	function add(array $params, $meta = []) { return $this->addItem($params,$meta); }
	function remove($id) {return $this->removeItem($id); }
	function total() {return $this->cart->total; }
	function items() {return $this->cart->items;}
	function cart() {return $this->cart;}
	function num_items() {return $this->cart->num_items;}
	
	// Marks items for checkout..
	function setOrderGroup($id) {
		$this->orderGroup = $id;
		$this->cart->items()->update(['pay_id'=>$id]);
		$this->cart->load('items');	// reload due to update..  @TODO feel like doing it this way is kludgie, need to explore other methods.
	}
	
	function getVendorGroup() {
		return $this->vendorGroup;
	}
	
	function split_meta($model, $params) {
		$attr = $model->getFillable();
		$result['model'] = array_key_extract($attr,$params);
		$result['meta'] = array_diff_key($params,$result['model']);
		return $result;
	}
	
	function getItemModel() {
		return $this->itemModel;
	}
	
	function getVendorChargeGroupModel() {
		return $this->vendorChargeGroupModel;
	}
	
	function getInvoiceModel() {
		return config('shoppingcart.models.invoice',Owens\ShoppingCart\Models\Invoice::class);
	}
	
	function addItem(array $params,  array $meta = []) {
		$item = (new ReflectionClass($this->getItemModel()))->newInstance();
		
		/*
		$attr = $item->getFillable();
		$insarray = array_key_extract($attr,$params);
		$diffs = array_diff_key($params,$insarray);
		*/
		
		$result = $this->split_meta($item,$params);
		
		$metas = array_merge($meta,$result['meta']);
		$result['model']['cart_id'] = $this->cart->id;
		$item->fill($result['model']);
		$item->setMeta($metas);
		$item->save();
		$this->itemAdded($item,true);
		Event::fire('cart.add',$item);
		event(new ShoppingCartAddEvent($item));
		$this->cart->load('items');	// This must be done to reload cart items after adding something.
		return $item;
		return $this;
	}
	
	
	// @TODO need to be able to update  or create responses..
	function addResponseToItem($item,$meta) {
		$this->cart->setMeta($meta);
		$this->cart->meta->save();	// Not sure if this is right..
	}
	
	function getMemberKeyName() {
		return $this->member_key;
	}
	
	function getCookieName() {
		return $this->cookie_name;
	}
	
	function setCartCookie($cart_key) {
		Cookie::queue($this->getCookieName(),$cart_key,time()+60*60*24*30);
	}
	
	function makeCartKey() {
		return str_random(60);
	}
	
	function getCartCookieKey() {
		$cart_key = Cookie::get($this->getCookieName(),false);
		if (!$cart_key || strlen($cart_key) > 60) {
			$cart_key = $this->makeCartKey();
		}
		return $cart_key;
	}
	
	function getUserCart() {
		if (!Auth::user())
			return false;
		if (!$this->cart) {
			$this->cart = $this->getCartModel()::firstOrNew(['user_id'=>Auth::user()->id]);
			if (!$this->cart->key) {
				$this->cart->key = $this->makeCartKey();
				$this->setCartCookie($this->cart->key);
			}
			$this->cart->save();
		}
		return $this->cart;
	}
	
	/*
	 *  Merges the cart into the current cart;
	 *  
	 *  
	 *  @return cart1 merged with cart2
	 */
	function mergeCarts($cart) {
		foreach ($cart->items as $item) {
			$this->addItem($item->toArray(),$item->getMeta()->toArray());	
		}
		return $this->cart;
	}
	
	/*
	 *  get shopping cart.  Possibiliteis
	 *  	1) Logged in as member, doesn't have a cart.
	 *  	2) Not logged in doesn't have a cart.
	 *  	3) Logged in but already has a cart.
	 *  	4) Has a cart with items but wasn't logged in
	 *  	5) Has a cart with items and now logs in and has a member cart with items..
	 *  
	 *  @return mixed, the CartModel
	 */
	function getCart() {
		if (!$this->cart) {
			$user_cart = $this->getUserCart();
			$cart_key = $this->getCartCookieKey();
			if ($user_cart && $cart_key == $user_cart->key) // Current cart and member cart are the same.
				$this->cart = $user_cart;
			else {
				if ($cart_key)
					$cart = $this->getCartModel()::firstOrNew(
					['key'=>$cart_key]
				);
				if (!$user_cart) {  // No member cart so use this cart..
					$this->cart = $cart;
				} else if ($cart->wasRecentlyCreated) {  // Had member cart, but this cart was new so use member cart.
						$this->cart = $user_cart;
				} else {  // Have member cart and existing cart, keep member cart and merge items from current cart.
					$this->cart = $user_cart;
					$this->mergeCarts($cart);
					$cart->delete();
				}
				if (!$this->cart->{$this->getMemberKeyName()} && Auth::user()) {
					if (!$this->cart->{$this->getMemberKeyName()}) {
						$this->cart->{$this->getMemberKeyName()} = Auth::user()->id;
					}
				}
				$this->cart->save();
			}
		}
		$this->setCartCookie($this->cart->key);
		return $this->cart;
	}
	
	
	function cartTotal() {
		$sum = 0;
		foreach ($this->cart->items as $item)
			$sum += $item->price * $item->qty;
		return $sum;
	}
	
	function itemRemoved($item, $save = false) {
		$this->cart->total -= $item->price * $item->qty;
		$this->cart->num_items -= $item->qty;
		$item->delete();
		if ($save)
			$this->cart->save();
		return $this;
		
	}
	
	function empty() {
		foreach ($this->cart->items as $item) {
			$this->itemRemoved($item,false);
		}
		// These should already be 0 but lets be safe.
		$this->cart->total = 0;
		$this->cart->num_items = 0;
		$this->cart->save();
	}
	
	function itemAdded($item, $save = false) {
		if ($item) {
			$this->cart->num_items += $item->qty;
			$this->cart->total += $item->price * $item->qty;
			if ($save)
				$this->cart->save();
		}
	}
	
	function removeItem($id) {
		$item = $this->cart->items->where('id',$id)->first();
		if (!$item) 
			return false;
		$this->itemRemoved($item);
		$this->cart->save();
		return true;
	}
	
	/**
	 * 
	 * 
	 * 
	 * @TODO get country in member information..
	 * @TODO get processing fee and shipping fees..
	 * @TODO process cart checkout, make sure qty doesn't change during checkout???
	 * @TODO handle coupons...
	 * 
	 */
	
	function process($callback=false, $payment_type=false,$payment_id = false) {
		$member = Auth::user()->Buyer();
		$invoice = $this->create_invoice($payment_type);
		
		$this->CheckoutItems($invoice,$this->cart->items);
		$this->cart->save();
		$invoice->save();
		return $invoice->getInvoice();
	}
	
	function create_invoice($payment_type) {
		$iBuilder = new InvoiceBuilder(Auth::user()->Buyer());  // Don't like member here
		$member = Auth::user()->Buyer();
		$invoiceParams = [
				'buyer_id' => $member->id,
	//			'org_id' => Auth::user()->org_id,
				'first_name' => $member->first_name,
				'last_name' => $member->last_name,
				'address' => $member->address,
				'city' => $member->city,
				'state' => $member->state,
				'zip' => $member->zip,
				//				'country' => '',
				'subtotal' => 0,
				'shipping' => 0,
				'processing' => 0,
				'total' => 0,
				'payment_type' => ($payment_type)?$payment_type:null,
				'payment_id' => ($this->orderGroup)?$this->orderGroup:null,
		];
		$iBuilder->create($invoiceParams);
		return $iBuilder;
	}
	
	function CheckoutItem($iBuilder, $item) {
		$invoiceItem = $iBuilder->addItem($item);
		$this->itemRemoved($item);
		Event::fire('cart.purchase.item',$invoiceItem);
		event(new ShoppingCartPurcahseItemEvent($invoiceItem));
	}
	
	function CheckoutItems($iBuilder,$items) {
		foreach ($items as $item) {
			if ($this->orderGroup) {
				if ($item->pay_id != $this->orderGroup)
					continue;
			}
			$this->CheckoutItem($iBuilder,$item);
		}
	}
	
	function process_vendor_items($vendor, $iBuilder=false) {
		$items = $this->cart->items->where('product.seller_id',$vendor->seller_id);
		$vendor->invoice_id = $iBuilder->getInvoice()->id;
		$this->CheckoutItems($iBuilder,$items);
		return $iBuilder->getInvoice();
	}
	
	function process_vendors($payment_type=false) {
//		$member = Auth::user()->Member;
		if (!$this->vendorGroup) {
			throw new \Exception("Must first create a vendor group with ShoppingCart::CreateVendorGroup()");
		}
		$iBuilder = $this->create_invoice($this->vendorGroup->cc_processor);
		foreach ($this->vendorGroup->vendors as $vendor) {
			if ($vendor->source_transaction != null)
				$this->process_vendor_items($vendor,$iBuilder);
		}
		$this->cart->save();
		$iBuilder->save();
		$this->vendorGroup->invoice_id = $iBuilder->getInvoice()->id;	// won't have invoice_id until invoice is saved.
		$this->vendorGroup->save();
		return $iBuilder->getInvoice();
	}
	
	function getOrderGroup() {
		$comps = explode(' ', microtime());
		return "ORDER_" . $comps[1] .($comps[0] * 10000000000). str_random(5);
	}
	
	protected function addVendors() {
		if ($this->vendorGroup == false)
			return false;
		foreach($this->cart->items as $item) {
			$this->vendorGroup->addItemToVendors($item);
		}
	}
	
	function makeVendorGroup(array $options = []) {
		
		if (!array_key_exists('transfer_group',$options)) {
			$options['transfer_group'] = $this->getOrderGroup();
		}		
//		$this->vendorGroup = new VendorChargeGroup($options);
		$this->vendorGroup= (new ReflectionClass($this->getVendorChargeGroupModel()))->newInstance($options);
		
		$this->setOrderGroup($this->vendorGroup->transfer_group);
//		$vendorGroup->source_transaction = $source_transaction;		// @TODO This may not be needed anymore..
		return $this->vendorGroup;
	}
	
	function CreateVendorGroup(array $options = []) {
		$this->vendorGroup = $this->makeVendorGroup($options);
		$this->addVendors();
		return $this->vendorGroup;
	}
	
	function CreateVendorGroupFromCharge($charge) {
		return $this->CreateVendorGroup($charge->transfer_group, $charge->id);
	}
	
	public static function routes(array $options = []) {
		if (!key_exists('prefix',$options))
			$options['prefix'] = 'cart';
	/*
		if (!key_exists('namespace',$options))
			$options['namespace']="\Owens\ShoppingCart\Controllers";
	*/
		Route::group($options,function($router) {
			$router->post('/add/{id}','Cart\CartController@addToCart')->name('cart.add');
			$router->get('/show','Cart\CartController@showCart')->name('cart.show');
			$router->get('/remove/{id}','Cart\CartController@removeItem')->name('cart.remove.item');
			$router->get('/checkout','Cart\CartController@checkOut')->name('cart.checkout');
			$router->post('/checkout','Cart\CartController@post_checkOut');
			$router->get('/empty','Cart\CartController@emptyCart')->name('cart.empty');
		});
	}
	
	public static function invoiceRoutes(array $options=[]) {
		if (!key_exists('prefix',$options))
			$options['prefix'] = '/invoice';
		else
			$options['prefix'] = $options['prefix'] . '/invoice';
		Route::group($options,function($router) {
			$router->get('/show/{MyInvoice}','Cart\InvoiceController@showInvoice')->name('invoice.show');
		});
	}
	public static function orderRoutes(array $options=[]) {
		if (!key_exists('prefix',$options))
			$options['prefix'] = '/orders';
		else
			$options['prefix'] = $options['prefix'] . '/orders';
		Route::group($options,function($router) {
			$router->get('/','Cart\OrderController@index')->name('orders');
		});
	}
	
	public static function privateRoutes(array $options=[]) {
		self::invoiceRoutes($options);
		self::orderRoutes($options);	
	}
}