<?php
namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductType;
use Illuminate\Support\Facades\Log;

class CartModel extends Model {
	
	protected $cart_key = "key";
	protected $cart_user_id = "user_id";
	
	
	function __construct (array $attributes = []) {
		$this->fillable = array_merge([$this->cart_key, $this->cart_user_id],$this->fillable);
		parent::__construct($attributes);
		//		$this->item_model = config('shoppingcart.cart_item_model',CartItem::class);
		
	}
	
	
	
	function items() {
		return $this->hasMany($this->getItemModel(),'cart_id','id');
	}
	
	function getItemModel() {
		return $this->item_model;
	}
}