<?php
namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kodeine\Metable\Metable;

class CartItemModel extends Model {
	use Metable;
	
	protected $cart_id = "cart_id";
	
	function __construct(array $attributes = []) {
		$this->fillable = array_merge([$this->cart_id],$this->fillable);
		parent::__construct($attributes);
	}
	
}