<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;

class StripeConnect extends Model
{
	protected $fillable = [
			'access_token',
			'refresh_token',
			'stripe_publishable_key',
			'stripe_user_id',
			'scope',
			'user_id',
			'active',
			'seller_id',
	];
    //
}
