<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use Owens\ShoppingCart\Models\Traits\CartInhertable;

class Address extends Model
{
	use CartInhertable;
	
    //
    protected $table = "addresses";
    
    protected $cart_fillable = [
    	'owner_id',
    	'first_name',
    	'last_name',
    	'address_line1',
    	'address_line2',
    	'city',
    	'state',
    	'postal_code',
    	'country',
    	'phone',
    	'fax',
    ];
    
    
    function getZipAttribute() {
    	return $this->postal_code;
    }
    
    function getFullNameAttribute() {
    	return $this->first_name ." ".  $this->last_name;
    }
    
}
