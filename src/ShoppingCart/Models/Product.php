<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Carbon\Carbon;
use App\Models\Traits\OrgTrait;
use Owens\ShoppingCart\Models\Traits\Sellable;
use Owens\ShoppingCart\Models\Traits\CartInhertable;

class Product extends Model {
	
	use Sellable;
	use CartInhertable;
	
	protected $cart_fillable = [
			'product_type_id',
			'name',
			'description',
			'price',
			'created_by_id',
			'seller_id',
	];
	
	protected $dates = [
			'start',
			'end',
			'signup_expires'
	];
	
	protected $table = 'products';
	
	protected $seller_key = 'seller_id';
	
	function ProductType() {
		return $this->belongsTo(ProductType::class);
	}
	
	function user() {
		return $this->belongsTo('\App\User');
	}
	
	function scopeOfStyle($query,$name) {
		return $query->whereHas('ProductType.ProductStyle',function($q) use($name){
			$q->where('name',$name);
		});
	}
	function Style() {
		return $this->ProductType->ProductStyle();
	}
	
	function scopeOfType($query,$type) {
		return $query->where('product_type_id',$type);
	}
	
// Methods for working with Stripe Payments
/*
	function connected_account() {
		return $this->hasOne(StripeConnect::class,'seller_id','org_id');
	}
	
	function getStripeAccount() {
		return $this->connected_account->stripe_user_id;
	}
	function getSellerId() {
		return $this->org_id;
	}
	*/	

	
}