<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStyle extends Model
{
	protected $fillable = ['name'];
	public $timestamps = false;
    //
    
}
