<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class VendorCharge extends Model
{
	protected $fillable = [
		'stripe_user_id',
		'amount',
		'currency',
		'vendor_charge_group_id',
		'transfer_amount',
		'fee',
		'status',
		'stripe_id',
		'balance_transaction',
		'invoice_id',
		'description',
		'seller_id',
		'transfer_type',
		'cc_processor',
		'sale_amount',
	];
    //
    
	function connected_account() {
		return $this->hasOne(StripeFacade::getStripeConnectModel(),'seller_id','seller_id');
	}
	
}
