<?php
namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductType extends Model {
	protected $fillable = [
			'name'
	];
	
	function ProductStyle() {
		return $this->belongsTo(ProductStyle::class);
	}
	
}