<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use Stripe\Transfer as StripeTransfer;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class VendorChargeGroup extends Model
{
	
	protected $fillable = [
		'transfer_group',	
		'stripe_transaction_id',
		'invoice_id',
		'source_transaction',
		'transfer_type',
		'cc_processor',
	];
	
    function vendors() {
    	$this->setObserver();
    	return $this->hasMany(VendorCharge::class);
    }
    
    function addVendor(array $vendorOptions) {
   // 	$vendorOptions['vendor_charge_group_id'] = $this->id;
    	$vendor = new VendorCharge($vendorOptions);
    	$vendor->stripe_user_id = ($vendor->connected_account)?$vendor->connected_account->stripe_user_id:null;
    	
  //  	$vendor = VendorCharge::create($vendorOptions);
  //		$vendor->save();
    	$this->vendors->push($vendor);
    	return $vendor;
    }
    
    function addItemToVendors($item) {
    	if (($vendor = $this->vendors->where('seller_id',$item->getSellerId())->first()) == false) {
    		$vendorOptions = [
    				'seller_id' => $item->getSellerId(),
 //   				'stripe_user_id' => $item->getStripeAccount(),
    				'amount' => 0,
    				'sale_amount' => 0,
    				'transfer_amount' => 0,
    				'currency' => $item->product->currency?:'usd',		// Don't like this because of products using floating point amounts and stripe doesn't.
    				'transfer_group' => $this->transfer_group,
    				//						'org_id' => $item->Product->org_id,	// I could need this for teamplay1?
    		//						'stripe_transfer_group_id'=> $transferGroup->id,
    				'description' => 'sale',
    				'status' => 'unknown',
    				'transfer_type' => $this->transfer_type,
    				'fee' => 0,
    				'cc_processor'=>$this->cc_processor,
    		];
    		$vendor = $this->addVendor($vendorOptions);
    	}
    	$amount = ($item->getPrice() * $item->qty * 100);
    	$fee = StripeFacade::getFee($amount); ; 
    	$vendor->fee += $fee;
    	$vendor->sale_amount += $amount;
    	$vendor->amount += ($amount - $fee);
    	$vendor->transfer_amount += ($amount - $fee);
    }
    
    
    protected $vendorsObserverSet;
    
    protected function setObserver()
    {
    	if (!isset($this->vendorsObserverSet)) {
    		$this->saved(function ($model) {
    			$model->saveVendors();
    		});
    		$this->vendorsObserverSet = true;
    	}	
    }
    
    protected function saveVendors() {
    	foreach ($this->vendors as $vendor) {
    		// $vendor->vendor_charge_group_id = $this->id;
    		if ($vendor->isDirty()) {
    			$vendor->vendor_charge_group_id = $this->id;
    			$vendor->invoice_id = $this->invoice_id;
    			$vendor->save();
    		}
    	}
    }
    
    function doTransfer() {
    	$status = 'pending';
    	foreach ($this->vendors as $vendor) {
    		$vendor->transfer_amount = $vendor->amount; // - $vendor->fee;
    		$vendor->stripe_id = null;
    		$vendor->balance_transaction=null;
    		$vendor->invoice_id = $this->invoice_id;
    		echo "Transfer amount: " . $vendor->transfer_amount  . "\n";
    		if ($vendor->stripe_user_id) {
    			$metaData = [
    				'fee' => $vendor->fee,	
    				'invoice_id' => $this->invoice_id,
    			];
    			$options = [
    					"amount" => $vendor->transfer_amount,
    					"currency" => $vendor->currency,
    					"destination" => $vendor->stripe_user_id,
    					"transfer_group" => $vendor->transfer_group,
    					"source_transaction" => $this->source_transaction,
    					"metadata" => $metaData,
    			];
				print_r($options);
				if ($vendor->amount > 0) {
					try {
						$transfer = StripeTransfer::create($options);
						echo "Transfer\n";
						print_r($transfer);
						$vendor->status = 'success';
						$vendor->stripe_id = $transfer->id;
						$vendor->balance_transaction = $transfer->balance_transaction;
					} catch (\Exception $e) {
						$vendor->status = 'failed';
						echo "Transfer failed " . $e->getMessage() . "\n";
					}
				} else
					$vendor->status = 'success';
    		} else
    			$vendor->status = 'no account';
    			//			VendorCharge::create($vendor);
    	}
    	$this->save();
    	return true;
    }
    
    function doTransferReverse() {
    	
    }
    
}
