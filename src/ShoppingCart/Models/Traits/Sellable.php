<?php
namespace Owens\ShoppingCart\Models\Traits;

trait Sellable {
	
	// Seller Traits..
	
	function Seller() {
		return $this->belongsTo($this->getSellerModel(),$this->SellerKeyy());
	}
	
	function scopeOfSeller($query,$seller_id) {
		return $query->where($this->getSellerKey(),$seller_id);
	}
	
	function getSellerKey() {
		return isset($this->seller_key)?$this->seller_key:'seller_id';
	}
	
	function getSellerModel() {
		return config('shoppingcart.models.seller','App\User');
	}
	
	
}