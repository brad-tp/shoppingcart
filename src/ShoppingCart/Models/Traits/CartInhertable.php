<?php

namespace Owens\ShoppingCart\Models\Traits;

trait CartInhertable {
	function __construct(array $attributes=[]) {
		if (isset($this->fillable)) {
			$this->fillable = array_merge($this->fillable,$this->cart_fillable);
		} else
			$this->fillable = $this->cart_fillables;
		parent::__construct($attributes);
	}
	
}