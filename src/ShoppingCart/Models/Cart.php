<?php
namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductType;
use Illuminate\Support\Facades\Log;
use Owens\ShoppingCart\Models\CartModel;
use Owens\ShoppingCart\Models\Traits\CartInhertable;

class Cart extends Model {
	
	use CartInhertable;
	
	protected $cart_fillable = [
			'key',
			'user_id',
	];
		
	function items() {
		return $this->hasMany($this->getItemModel(),'cart_id','id');
	}
	
	function getItemModel() {
		return config('shoppingcart.models.cart_item',CartItem::class);
	}
	
	// Temporary accessor functions until I change it to work with items.
	function getNumItems() {
		return $this->num_items;
	}
	
	function getTotal() {
		return $this->total;
	}
	
	function vendors() {
		return $this->hasManyThrough('\Owens\Models\CartItem','\App\Models\Org');
	}
}