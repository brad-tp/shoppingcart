<?php
namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kodeine\Metable\Metable;
use Owens\ShoppingCart\Models\Traits\CartInhertable;

class CartItem extends Model {
	use Metable;
	use CartInhertable;
	
	protected $cart_fillable = [
			'cart_id',
			'product_id',
			'qty',
//			'for_member_id',
			'org_id',
			'description',
			'pay_id',
			'product_name',
	];
	
	protected $metaTable = 'cart_items_meta';
	
	function Product() {
		return $this->belongsTo($this->getProductModel());
	}
		
	public function getProductTypeNameAttribute() {	// This is wrong..
		return $this->Product->name;
	}
	
	// Methods for cart.
	protected function getProductModel() {
		return config('shoppingcart.models.product');
	}
	
	// Methods needed for StripeCheckout..
	
	public function getSellerId() {
		return $this->Product->seller_id;
	}
	
	public function getPrice() {
		return $this->Product->price;
	}
}