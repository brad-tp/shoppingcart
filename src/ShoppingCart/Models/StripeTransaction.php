<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;

class StripeTransaction extends Model
{
	protected $fillable = [
		'stripe_id',
		'object',
		'amount',
		'currency',
		'balance_transaction',
		'invoice_id',
		'transfer_group',
		'stripe_transfer_group_id',
	];
    //
}
