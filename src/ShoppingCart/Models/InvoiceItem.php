<?php
namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use Kodeine\Metable\Metable;
use Owens\ShoppingCart\Models\Traits\CartInhertable;

class InvoiceItem extends Model {
	use Metable;
	use CartInhertable;
	
	protected $seller_model = 'App\Models\Org';
	
	protected $cart_fillable = [
			'member_id',
			'org_id',
			'invoice_id',
			'product_name',
			'description',
			'qty',
			'price',
			'total',
//			'for_member_id',
			'product_id',
			'product_type',
			'buyer_id',
			'seller_id'
	];
	
	protected $table = "invoice_items";
	protected $metaTable = 'invoice_items_meta';
	
	function Org() {
		return $this->belongsTo('App\Models\Org');
	}
	
	function ForMember() {
		return $this->hasOne('App\Models\Member','id','for_member_id');
	}
	
	function Product() {
		return $this->belongsTo($this->getProductModel());
	}
	
	/*
	function ProductOrg() {
		return $this->belongsTo('App\Models\Org','seller_id');
	}
	*/
	
	// Cartable model methods
	function seller() {
		return $this->belongsTo($this->getSellerModel(),'seller_id');
	}
	
	
	function getSellerModel() {
		return config('shoppingcart.models.seller',$this->seller_model);
	}
	
	function getProductModel() {
		return config('shoppingcart.models.product','Owens\ShoppingCart\Models\Product');
	}
	
}