<?php

namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\OrgTrait;

class PendingStripeConnect extends Model
{	
	protected $fillable = [
			'user_id',
			'access_token',
			'refresh_token',
			'stripe_publishable_key',
			'stripe_user_id',
			'scope',
			'key',
			'accepted_at',
			'seller_id',
	];
	
    //
    public function user() {
    	return $this->belongsTo('App\User');
    }
}
