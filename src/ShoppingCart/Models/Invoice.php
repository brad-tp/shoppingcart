<?php
namespace Owens\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductType;
use Owens\ShoppingCart\Models\Traits\CartInhertable;

class Invoice extends Model {
	use CartInhertable;
	
	protected $cart_fillable = [
			'member_id',
			'total',
			'org_id',
			'first_name',
			'last_name',
			'address',
			'city',
			'state',
			'zip',
			'country',
			'subtotal',
			'shipping',
			'processing',
			'payment_type',
			'payment_id',
			'coupon_code',
			'coupon_discount',
			'buyer_id'
	];
	
	protected $itemsObserverSet;
	protected $table = "invoices";
	
	/* Cart models */
	function getInvoiceItemModel() {
		return config('shoppingcart.models.invoice_item',InvoiceItem::class);
	}
	
	function getAddressModel() {
		return config('shoppingcart.models.address',Address::class);
	}
	
	function getVendorChargeModel() {
		return config('shoppingcart.models.vendor_charge',VendorCharge::class);
	}
	function getBuyerModel() {
		return config('shoppingcart.models.buyer','App\User');
	}
	
	function getSellerModel() {
		return config('shoppingcart.models.seller','App\User');
	}
	
	// End Cart models */
	
	function InvoiceItems() {
		return $this->hasMany($this->getInvoiceItemModel());
	}
	
	function items() {
		$this->setObserver();
		return $this->hasMany($this->getInvoiceItemModel());
	}
	
	function shipping_address() {
		return $this->belongsTo($this->getAddressModel(),'shipping_address_id');
	}
	
	function billing_address() {
		return $this->belongsTo($this->getAddressModel(),'billing_address_id');
	}
	
	protected function AddAddress($options) {
		return $this->getAddressModel()::create($options);
	}
	
	function AddShippingAddress($options) {
		$address = $this->AddAddress($options);
		$this->shipping_address_id = $address->id;
		$this->save();
	}
	
	function AddBillingAddress($options) {
		$address = $this->AddAddress($options);
		$this->billing_address_id = $address->id;
		$this->save();
	}
	
	
	function buyer() {
		return $this->belongsTo($this->getBuyerModel(),'buyer_id');
	}
	
	function sellers() {
		return $this->hasManyThrough($this->getSellerModel(),$this->getInvoiceItemModel(),'invoice_id','id','id','seller_id')->distinct();
	}
	
	// Vendorable
	function charges() {
		return $this->hasMany($this->getVendorChargeModel());
	}
	
	// Invoiceable...
	function addItem(array $options=[], array $meta = []) {
		$item = new InvoiceItem($options);
		$item->setMeta($meta);
		$this->items->push($item);
		$this->total += $item->total;
		$this->subtotal += $item->total;
		
		return $item;
	}
	
	protected function setObserver()
	{
		if (!isset($this->itemsObserverSet)) {
			$this->saved(function ($model) {
				$model->saveItems();
			});
				$this->itemsObserverSet = true;
		}
	}
	
	protected function saveItems() {
		foreach ($this->items as $item) {
			if ($item->isDirty()) {
				$item->invoice_id = $this->id;
				$item->save();
			}
		}
	}
	
	
}