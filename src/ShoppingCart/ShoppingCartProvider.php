<?php

namespace Owens\ShoppingCart;

// use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;

class ShoppingCartProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
	
	protected $namespace = "Owens\ShoppingCart\Controllers";
	
    public function boot()
    {
        //
        parent::boot();
 //       $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->publishes([__DIR__ . '/config/shoppingcart.php' => config_path('shoppingcart.php')], 'config');
        $this->publishes([__DIR__ . '/../stubs/Controllers/Cart' => app_path('/Http/Controllers/Cart')], 'controllers');
        $this->publishes([__DIR__ . '/../database/migrations' => database_path('/migrations')], 'migrations');
        
    }
    
    protected function mapCartRoutes()
    {
    	Route::middleware('web')
    	->namespace($this->namespace)
    	->group(__DIR__ .'/routes/cartweb.php');
    }
    

    function map() {
    	$this->mapCartRoutes();
    }
    
    public function setRouteBindings() {
    	Route::bind('MyInvoice',function($value) {
    		return  config('shoppingcart.models.invoice',\Owens\ShoppingCart\Models\Invoice::class)
    			::where('id',$value)->where('buyer_id',\Auth::user()->getBuyerId())->first();
    	});
    }
    
    public function setBindings() {
    	$this->app->bind("shoppingcart",function() {
    		return new \Owens\ShoppingCart\ShoppingCart(Config::get('shoppingcart'));
    	});
    		
    }
    
    function setModelBindings() {
    	Route::model('product',config('shoppingcart.models.product'));
    	Route::model('invoice',config('shoppingcart.models.invoice',\Owens\ShoppingCart\Models\Invoice::class));
    	
    	
    }
    
    public function setConsole() {
    	if ($this->app->runningInConsole()) {
    		$this->commands([
    				\Owens\ShoppingCart\Console\ListCommand::class,
    		]);
    	}
    	
    }
    
    public function loadAliases() {
    	$loader = \Illuminate\Foundation\AliasLoader::getInstance();
    	$loader->alias('ShoppingCart', \Owens\ShoppingCart\Facade\ShoppingCart::class);
    }
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->mergeConfigFrom( __DIR__ ."/config/shoppingcart.php", "shoppingcart",".php" );
        $this->loadAliases();
        $this->setConsole();
        $this->setBindings();
       	$this->setRouteBindings();
       	$this->setModelBindings();
        View::addLocation(__DIR__ ."/../resources/views");
    }
}
