<?php 


return [
		'test'=>'Testing',
		'models' => [
			'cart'=> Owens\ShoppingCart\Models\Cart::class,
			'cart_item' => Owens\ShoppingCart\Models\CartItem::class,
			'product' => Owens\ShoppingCart\Models\Product::class,
			'seller' => App\Models\Org::class,
			'vendor_charge_group' => Owens\ShoppingCart\Models\VendorChargeGroup::class,
			'invoice' => Owens\ShoppingCart\Models\Invoice::class,
			'invoice_item' => Owens\ShoppingCart\Models\InvoiceItem::class,
			'address' => Owens\ShoppingCart\Models\Address::class,
			'vendor_charge' => Owens\ShoppingCart\Models\VendorCharge::class,
			'buyer' => App\User::class,
			
		],
		'layouts' => [
				'default' => 'layouts.cart_layout',
			],
		'stripe' => [
				'model' => App\User::class,
				'key' => env('STRIPE_KEY'),
				'secret' => env('STRIPE_SECRET'),
				'wh_key' => env('STRIPE_WH_KEY'),
				'client_id' => env('STRIPE_CLIENT_ID'),
				'vendor_mode' => env('STRIPE_VENDOR_MODE','direct'),
				'account' => env('STRIPE_ACCOUNT',"acct_1BtLrMLuAVFeSRbZ"),
		],
		
];
