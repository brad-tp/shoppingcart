<?php

namespace Owens\ShoppingCart\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use \URL;

use Auth;

//@TODO don't like putting auth allocation here but it is used everywhere
// So this will be the simpliest way keep it going during porting time..
// 

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    function __construct() {
    }
    
    function validate(Request $request, $args) {
    	$request->validate($args);
    }
    
}
