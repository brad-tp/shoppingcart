<?php

namespace Owens\ShoppingCart\Controllers\Cart;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class InvoiceController extends BaseController
{
	protected $invoice_view = 'invoice.show';
	//
	
	function getInvoiceView() {
		return (isset($this->invoice_view))?$this->invoice_view:'invoice.show';
	}
	
	function showInvoice(Request $request,$invoice) {
		return view($this->getInvoiceView(),[
				'invoice' => $invoice,
		]);
	}
}
