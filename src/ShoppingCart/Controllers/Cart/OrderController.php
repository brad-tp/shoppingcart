<?php

namespace Owens\ShoppingCart\Controllers\Cart;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Owens\ShoppingCart\Facade\ShoppingCart as ShoppingCartFacade;

use App\Models\Invoice;

class OrderController extends BaseController
{
	protected $view = 'orders.index';
	
	protected function getView() {
		return (isset($this->view))?$this->view:'orders.index';
	}
	
	function buyerOrders() {
		$invoices=Invoice::where('buyer_id',\Auth::user()->getBuyerId())->get();
		return view($this->getView(),['invoices'=>$invoices]);
	}
	
	function sellerOrders() {
		$invoices=Invoice::where('buyer_id',\Auth::user()->getBuyerId())->get();
		return view($this->getView(),['invoices'=>$invoices]);
	}
	
	function index() {
		$invoices=ShoppingCartFacade::getInvoiceModel()::where('buyer_id',\Auth::user()->getBuyerId())->get();
		return view($this->getView(),['invoices'=>$invoices]);
	}
}
