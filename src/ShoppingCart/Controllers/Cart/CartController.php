<?php

namespace Owens\ShoppingCart\Controllers\Cart;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use Owens\ShoppingCart\Controllers\Controller;
use Owens\ShoppingCart\Facade\ShoppingCart;

use App\Models\Member;
use Owens\ShoppingCart\Traits\Controllers\Cartable;

class CartController extends BaseController
{
	use Cartable;
	
	/*
	function localPrepareCartContent($options, $product) {
		
		$forWho = false;
		if (!isset($options['for_member_id']))
			$options['for_member_id'] = Auth::user()->member_id;
		else
			$forWho = $options['for_member_id'];
		$forMember = Member::find($options['for_member_id']);
		$options['description'] = $product->description;  // Not sure I like it this way, maybe need short description for product.
		$options['org_id'] = Auth::user()->org_id;
		$options['test'] = 'test';
		if ($forWho) {
			$options['description'] = "For: " . $forMember->first_name . " " . $forMember->last_name;
			$options['for'] = $forWho;
		}
		return $options;
	}
	
	function localVerifyCart($options,$product) {
		$forWho = null;
		if (isset($options['for_member_id']))
			$forWho = $options['for_member_id'];
		if ($forWho == -1) {
			return "Must select who the product is for, if your child isn't in the list select \"Add Child\"";
		}
		return true;
	}
	*/
}
