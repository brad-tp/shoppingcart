<?php 

namespace Owens\ShoppingCart\Facade;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Route;

class ShoppingCart extends Facade {
	
	protected static function  getFacadeAccessor() {
		return "shoppingcart";
	}
	
//	static function routes() {
//		self::myroutes();
//	}
}
