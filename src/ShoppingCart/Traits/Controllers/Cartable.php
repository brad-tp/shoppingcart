<?php

namespace Owens\ShoppingCart\Traits\Controllers;

use Owens\Useful\Traits\Controller\ControllerTraits;
use Illuminate\Http\Request;
use Owens\ShoppingCart\Facade\ShoppingCart;

trait Cartable {
	
	use ControllerTraits;
	
	
	function validate(Request $request, $args) {
		$request->validate($args);
	}
	
	function prepareCartContent($options,$product) {
		return $this->callUserMethodWithDefault('localPrepareCartContent',$options,$options,$product);
	}
	
	function verifyCart($options,$product) {
		return $this->callUserMethodWithDefault('localVerifyCart',true,$options,$product);
	}
	
	function addToCartAction() {
		return $this->flashBackInfo("Added to cart");
	}
	
	function getProductModel() {
		return config('shoppingcart.models.product',Owens\ShoppingCart\Models\Product::class);
	}
	
	function getCartBasicOptions($request, $product) {
		$params = $request->all();
		$params['qty'] = $request->input('qty',1);
		$params['product_name'] = $product->name;
		unset($params['_token']);
		return $params;
	}
	
	// @TODO validate product information, etc..
	// @TODO clean up how products are added..
	
	function addToCart(Request $request) {
		$this->validate($request,[
				'product_id' => 'required',
		]);
		$product = $this->getProductModel()::find($request->product_id);
		if (!$product) {
			return $this->flashBackError("Invalid product selection");
		}
		$params = $this->getCartBasicOptions($request,$product);
		if (($message = $this->verifyCart($params,$product)) !== true)
			return $this->flashBackError($message);
			$options = $this->prepareCartContent($params, $product);
			//print_r($params);
			ShoppingCart::add($options);
			return $this->addToCartAction();
	}
	
	function removeItem(Request $request,$id) {
		ShoppingCart::remove($id);
		return redirect()->back();
	}
	
	function showCart(Request $request) {
		$cart = ShoppingCart::getCart();
		return view("cart/show", [
				"cart" => $cart
		]);
	}
	
	function checkOut(Request $request) {
		$cart = ShoppingCart::getCart();
		return view("cart/checkout", [
				"cart" => $cart
		]);
	}
	
	function testCoupon() {
		
	}
	
	// @TODO handle coupons in invoices..
	function post_checkOut(Request $request) {
		echo "post checkout";
		$coupon_code = $request->coupon_code;
		if ($coupon_code != 'teamplay1') {
			return $this->flashBackError('Must have a valid coupon code to proceed at this time to proceed');
		}
		$invoice = ShoppingCart::process();
		return redirect()->route('invoice.show',[
				'invoice' => $invoice->id
		]);
	}
	
	function emptyCart() {
		ShoppingCart::empty();
		return redirect()->back();
		
	}
	
}