<?php

/**
 * 
 */

namespace Owens\ShoppingCart;



use Owens\ShoppingCart\ShoppingCartContract;

// Base class of manager.. Not sure what all it will do yet.
/**
 * 
 * @author thrasher
 *
 */
abstract class ShoppingCartBase implements ShoppingCartContract {
	protected $user;
	protected $errors;
	protected $cart_model = false;
	
	function __construct(array $config = []) {
		$this->errors = [];
		$this->config = array_dot($config);
		$this->init();
	}
	
	/**
	 * 
	 * @return \Owens\ShoppingCart\ShoppingCartBase
	 */
	function get() {
		return $this;
		
	}
	/*
	static function get() {
		$obj = new static();
		$obj->init();
		return $obj;
	}
	*/
	
	/**
	 * 
	 * @return array of errors
	 */
	function getErrors() {
		return $this->errors;
	}
	
	function init() {
		
	}
	
	/**
	 * 
	 * @return model being used in cart, false if no model set.
	 */
	function getCartModel() {
		return $this->cart_model;
	}
	
	function getConfig($name,$default=false,$required = false) {
		$item = $default;
		if (isset($this->config[$name]))
			$item = $this->config[$name];
		else if ($required)
			throw new ConfigurationException("Missing configuration $name");
		return $item;
	}
	
	abstract public function add(array $params, $meta = []);
	abstract public function remove($id);
	abstract public function total();
	abstract public function empty();
	abstract public function items();
	abstract public function cart();
	
	
}
