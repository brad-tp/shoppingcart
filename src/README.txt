
Shopping Cart
================

composer require

php artisan vendor:publish --provider Owens\ShoppingCart\ShoppingCartProvider

publishes: 
	App/Http/Controllers/Cart		--tag=controllers
	config/shoppingcart.php			--tag=config
	database/migrations				--tag=migratons

Modify any migration files so data can be moved over, for instance seller_id and buyer_id may need to be set from existing data.

php artisan vendor:publish --provider Owens\Stripe\StripeProvider

publish:
	App/Http/Controllers/Stripe		--tag=controllers
	resource/views/owens/stripe 	--tag=views
	public/js/stripe.js				--tag=javascript



Set layout in config\shoppingcart.php

Configure Stripe -- See below section.

User Model modifications.
Add getBuyerId() and getSellerId() methods to UserModel.
Add setStripConnect() method.
Add Buyer() method to user.  More than likely just return $this but if you want to use a different model than user return that.

Add use Billable (for cashier).
Set your seller model in "config/shoppingcart.php"

Put stripe.js into place (This needs to be made to be published).

Testing Stripe Connect
========================
go to http//yoursite/stripe/connect

Problems.
--------------
Still have problem with "Member" being used.

Product types are not populated.
--------------------------------

Try and connect.

Testing shopping cart.
=========================

Add an item into the cart and see if it is there.





Shopping cart is meant to work with the Auth model of laravel.

Creating shopping cart models
=================================
A shopping cart consists of 3 components.
	1) Cart
	2) CartItems
	3) CartItems Meta data.
	
Creating a cart requires the cart to extend CartModel, and the items in the cart needs to extend CartItemModel.

Cart
=================================

Cart is required to have the following minimum fields.
	<id,key,user_id,num_items,total>.
	
	To override the name of the key set
		protected $cart_key = "key";
		protected $cart_user_id = "user_id";
	
CartItem
============================
CartItem is required to  to have the minimum fields
	<id,cart_id,qty,price,description>
	
	For meta data Kodine\Metable is used.  See documents for that at http://....
	
	
Stripe
===========================================
Steps for setting up stripe
	1) Create account
	2) Configure Keys.
		STRIPE_KEY=<publishable_key>
		STRIPE_SECRET=<secret_key>
		STRIPE_WH_KEY=<webhook_key>
		STRIPE_CLIENT_ID=<client_id>
		STRIPE_ACCOUNT=<your_account>
		
		publishable_key		-- from stripe => developers => API Keys
		secret_key			-- from stripe => developers => API Keys
		webhook_key			-- from stripe => developers => Webhooks
									+ Add End Point
									Enter URL <http://<your_server>/stripe/webhook
									Click your new endpoint and get signing secret.
		client_id			-- from strip => Connect => Settings => client ID
									Add URI for redirects
									https://<your_server>/stripe/connect/onboard
				
	3) Add service provider to config/app.php.
		Laravel\Cashier\CashierServiceProvider::class,
    	Owens\Stripe\StripeProvider::class,
    	
    4) Add stripe.js to your public/js directory. 
    
 Database
 ====================
 	Tables Created/Modified:
 		1) Users 				-- Added Cashier info
 		2) stripe_subscriptons 	-- Created for Cashier
 		3) stripe_transactions 	-- Keep Track of stripe transactons
 		4) pending_stripe_infos -- Keep Connect information from connect responsed until email activation (Need to change this name)
 		5) vendor_charge_groups -- Group of vendors for a checkout
 		6) vendor_charge 		-- Each vendor in the charge group
 		7) products 			-- Products that are being soled
 		8) invoices 			-- Invoices from a checkout
 		9) invoice_items 		-- Items that are in an invoice (also invoice_items_meta since it is metable)
 		10) stripe_connects 	-- Active stripe connect accounts
 		11) addresses 			-- Address that are used (e.g. invoice keeps billing and shipping addresses)
 		12) product_type 		-- Type of product being sold (e.g. Team, Class, Merchandise)
 		13) product_style 		-- Style of product (e.g. membership, merchandise, etc..)
 		14) carts 				-- Shopping cart for a customer
 		15) cart_items 			-- Items in the cart (also cart_items_meta since it is metable).
    
 Add any needed models
 ============================
 
 							
		
Methods needed:
================================================
	User Model
	===================
		getSellerId()				-- Needs to return the ID value of the seller that is setting up connect.
		getBuyerId() 				-- Needs to return the ID value of the buyer.  This way it can be something different that user_id.
		Buyer()						-- Needs to return the Buyer, more than likey it is just return $this.
		setStripConnect($connect)	-- Sets the stripe_connect_id.  Might go away, not being used.
		getStripeAccount()			-- Returns the stripe account for the user.
		
	Product Model
	====================
		need to use \Owens\ShoppingCart\Models\Sellable

Fix Views
==============================
	Fix any views in the owens directory to display how you would like.  First thing to fix is the layout they are using.
