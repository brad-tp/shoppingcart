<?php

namespace Owens\Stripe;

use App\Http\Controllers\Traits\ControllerTraits;

use App\Models\VendorChargeGroup;
use App\Models\StripeTransaction;
use App\Models\VendorCharge;


use Stripe\Charge as StripeCharge;
use Stripe\Transfer as StripeTransfer;
use Stripe\Account;
use Stripe\Card as StripeCard;
use Stripe\Token as StripeToken;

use Owens\Stripe\Events\ChargeSucessfulEvent;
use Owens\ShoppingCart\Facade\ShoppingCart;
use Owens\Stripe\Stripe;
use Owens\Stripe\Exceptions\StripeException;
use Owens\Stripe\StripeConnectCharge;
use Owens\Stripe\Facade\Stripe as StripeFacade;
use Owens\Stripe\Events\InvoiceGeneratedEvent;

class StripeConnectCharge {
	
		
//	protected $stripe;
	protected $buyer;
	protected $token;
	protected $card_id;
	protected $card;
	protected $vendorGroup;
	protected $customer;
	protected $helper;
	protected $invoice=false;
	protected $errors = [];
	protected $msg;
	
	function __construct($helper, $buyer, $token=false, $card_id=false) {
	//	$this->stripe = resolve('Owens\Stripe\Stripe');
		$this->buyer = $buyer;
		$this->token = $token;
		$this->helper = $helper;
		$this->card_id = $card_id;
		$this->vendorGroup = $this->create_vendor_group($this->getOrderGroup());
		$this->customer = $this->getStripeCustomer($token);
		$this->card = $this->getCustomerCard($this->customer,$card_id);
	}
	
	function Charge() {
		$method = camel_case(StripeFacade::getChargeType() ."_charge");
		if (method_exists($this,$method))
			$this->{$method}();
		else
			throw new StripeException("No valid charge method: $method");
		$this->invoice->addBillingAddress($this->getBillingAddress());
		$this->invoice->addShippingAddress($this->getShippingAddress());
		event(new InvoiceGeneratedEvent($this->invoice));
		
	}
	
	function getInvoice() {
		return $this->invoice;
	}
	
	function getUserMetas() {
		if ($this->helper && method_exists($this->helper,"getMetas"))
			return $this->helper->getMetas(); 
		return [];
	}
	
	function getMetaData(array $metaOptions = []) {
		$metadata = [
				'buyer_id' => \Auth::user()?\Auth::user()->getBuyerId():null,
				'chargeMethod'=>StripeFacade::getChargeType(),
		];
		$userMetas = $this->getUserMetas();
		return array_merge($metadata,$metaOptions,$userMetas);
	}
	
	function callHelper($method,...$args) {
		if ($this->helper && method_exists($this->helper,$method))
			return $this->helper->{$method}(...$args);
		return [];
	}
	
	function getBillingAddress() {
		$address=$this->callHelper('getBillingAddress',$this->buyer);
		if (!array_key_exists('owner_id',$address))
			$address['owner_id'] = $this->buyer->id;
		return $address;
	}
	
	function getShippingAddress() {
		$address = $this->callHelper('getShippingAddress',$this->buyer);
		if (!array_key_exists('owner_id',$address))
			$address['owner_id'] = $this->buyer->id;
		return $address;
	}
	
	function getDestination($account, $amount) {
		if ($account == null)
			return false;
		$transfer_amount = $amount - StripeFacade::getFee($amount);
		if ($transfer_amount < 0)	// Make sure we don't end up with  negitive.
			$transfer_amount = $amount;
		$destination = [
				'amount' => $transfer_amount,
				'account' => $account,
		];
		return $destination;
	}
	
	/*
	function getStripeTransactionModel() {
		return '\App\Models\StripeTransaction';
	}
	*/
	
	function createStripeTransaction($result, $transferGroup) {
		$params = array_merge($result->__toArray(), [
				'stripe_id'=>$result->id,
				//		'invoice_id' => $invoice->id,
				'transfer_group' => $transferGroup->transfer_group,
				//			'stripe_transfer_group_id'=>$transferGroup->id,
				'stripe_transfer_group_id'=>1,					// Just stubbing this for now, may not be needed at all could set the transaction in the transfer group.
		]);
		return StripeFacade::getStripeTransactionModel()::create($params);
	}
	
	function handleChargeResponse($result=false, $transferGroup=false) {
		$this->invoice = ShoppingCart::process_vendors('stripe');
		return $this->invoice;
	}
	
	function freeCharge($options1, $options2=[]) {
		\Log::debug('free charge');
		$result = (object) ['id'=> "free_" . str_random(30)];
		$result = new \Stripe\StripeObject();
		$result->id = "free_" . str_random(30);
		$result->object = 'fcharge';
		$result->amount = $options1['amount'];
		$result->currency = $options1['currency'];
		$result->balance_transaction = 'tfxn_' .str_random(30);
		$result->transfer_group = $options1['transfer_group'];
		return $result;
	}

	function doCharge($args) {
		if ($args[0]['amount'] == 0)
			return $this->freeCharge(...$args);
		try {
			$result = StripeCharge::create (...$args);
			//	echo "Payment successful\n";
			//			print_r($result);
		} catch ( \Exception $e ) {
			$this->errors[] = $e->getMessage();
			\Log::info("Charge exceptions: " . print_r($args[0],true));
			return $e;
		}
		return $result;
	}
	
	function buildMultitransferPayload($vendor,$description) {
		$metaData = $this->getMetaData();
		$options = [
				"amount" => ShoppingCart::total() * 100,
				"currency" => "usd",
				//				"source" => $token, // obtained with Stripe.js
				'customer' => $this->customer->id,
				"description" => $description,
				"transfer_group" => $this->vendorGroup->transfer_group, // Use for group of transfers
				'metadata' => $metaData,
				
		];
		if ($this->card) 	// 	If a different card is selected
			$options['source'] = $this->card;
		return [$options];
	}
	
	function singleCharge() {
		$description = "Cart Checkout: " . date('c');
		$args=$this->buildPayload(null, $description);
		$result = $this->doCharge($args);
		if ($result instanceof \Exception) {
			throw new StripeException("StripeExcepiton",0,$result,'stripe.charge.fail');
		}
		$this->vendorGroup->source_transaction = $result->id;
		foreach ($this->vendorGroup->vendors as $vendor)
			$vendor->source_transaction = $result->id;
		$this->createStripeTransaction($result,$this->vendorGroup);
		$invoice = $this->handleChargeResponse($result, $this->vendorGroup);
		event(new ChargeSucessfulEvent($result));
		return true;
	}
	
	function multitransferCharge() {
		return $this->singleCharge();
		/*
		$description = "Cart Checkout: " . date('c');
		$args = $this->buildPayload(null,$description);
		$result = $this->doCharge($args);
		if ($result instanceof \Exception) {
			throw new StripeException("StripeExcepiton",0,$result,'stripe.charge.fail');
		}
		$this->vendorGroup->source_transaction = $result->id;
		foreach ($this->vendorGroup->vendors as $vendor)
			$vendor->source_transaction = $result->id;
		$this->createStripeTransaction($result,$this->vendorGroup);
		$invoice = $this->handleChargeResponse($result, $this->vendorGroup);
		event(new ChargeSucessfulEvent($result));
		return true;
		 */
	}
	
	function standaloneCharge() {
		echo "<PRE>";
		echo "Standalone\n";
		return $this->singleCharge();
		/*
		$description = "Cart Checkout: " . date('c');
		$args=$this->buildPayload(null, $description);
		$result = $this->doCharge($args);
		if ($result instanceof \Exception) {
			throw new StripeException("StripeExcepiton",0,$result,'stripe.charge.fail');
		}
		foreach ($this->vendorGroup->vendors as $vendor)
			$vendor->source_transaction = $result->id;
		$this->createStripeTransaction($result,$this->vendorGroup);
		$invoice = $this->handleChargeResponse($result, $this->vendorGroup);
		event(new ChargeSucessfulEvent($result));
		return true;
		*/
	}
	
	
	
	function buildDestinationPayload($vendor, $description) {
		$destination = $this->getDestination($vendor->stripe_user_id,$vendor->sale_amount);
		$metaData = $this->getMetaData();
		$options = [
				"amount" => $vendor->sale_amount,
				"currency" => "usd",
				'customer' => $this->customer->id,
				"description" => $description,
		//		"destination" => $destination, 	// Use for direct charge
				"transfer_group" => $this->vendorGroup->transfer_group,
				'metadata' => $metaData,
		];
		if ($destination)
			$options['destination'] = $destination;
		if ($this->card) 	// 	If a different card is selected
			$options['source'] = $this->card;
			
		return [$options];
	}
	
	function buildDirectPayload($vendor, $description) {
		$token = $this->createCustomerToken($vendor->stripe_user_id);
		$metaData = $this->getMetaData();
		$options = [
				"amount" => $vendor->sale_amount,
				"currency" => "usd",
				"source" => $token, // obtained with Stripe.js
				"description" => $description,
				'metadata' => $metaData,
				'application_fee' => $vendor->fee,
		];
		$args = [
				$options,
				['stripe_account'=>$vendor->stripe_user_id],
		];
		return $args;
	}
	
	function buildPayload($vendor,$description) {
		$method=camel_case("build_" .StripeFacade::getChargeType(). "_payload" );
		if (method_exists($this,$method)) {
			return $this->{$method}($vendor,$description);
		} else
			throw new StripeException("No Payload method: " . $method);
	}
	
	function makeCharge($vendor, $description) {
		$args = $this->buildPayload($vendor,$description);
		$result = $this->doCharge($args);
		if (!($result instanceof \Exception)) {
			$vendor->source_transaction = $result->id;
			$vendor->status="success";
		} else
			$vendor->status="fail";
		return $result;
	}
	
	function getCustomerCard($customer, $card_id=false) {
		if (!$card_id)
			return false;
		return $customer->sources->retrieve($card_id);
	}
	
	function finishCharge($passed,$vendorGroup=false) {
		if ($passed == 0)
			return $this->flashBackError($this->errors[0]);
		if ($passed != count($this->vendorGroup->vendors)) {
			$msg = "Not all items were able to be purchased: " . $this->errors[0];
		} else
			$msg = "Payment successful";
		$invoice = $this->handleChargeResponse();
		return $invoice;
	}
	
	
	function vendorCharge() {
		$description = "Cart Checkout: " . date('c');
		if ($this->vendorGroup->vendors->count() == 0) {
			throw new StripeException("No vendor items in cart",0,null,"no.vendors");
			// return $this->handleFail('no.vendors','No vendor items in cart');
		}
		$passed = 0;
		foreach ($this->vendorGroup->vendors as $vendor) {
			//	$result = $this->doDirectCharge($vendor,$description);
			$result = $this->makeCharge($vendor,$description);
			
			if (!($result instanceof \Exception)) {
				$this->createStripeTransaction($result,$this->vendorGroup);
				$passed++;
			} else if ($passed == 0) {	//Don't continue if first on fails.
				throw new StripeException("Charge Failed",0,$result,"stripe.charge.fail");
			}
		}
		//		event(new ChargeSucessfulEvent($result));
		return $this->finishCharge($passed,$this->vendorGroup);
	}
	
	function destinationCharge() {
		return $this->vendorCharge();
	}
	
	function directCharge() {
		return $this->vendorCharge();
	}
	
	function buildStandalonePayload($vendor, $description) {
		$metaData = $this->getMetaData();
		$options = [
				"amount" => ShoppingCart::total() * 100,
				"currency" => "usd",
				"customer" => $this->customer->id,
				"description" => $description,
				'metadata' => $metaData,
				"transfer_group" => $this->vendorGroup->transfer_group,
		];
		if ($this->card) 	// 	If a different card is selected
			$options['source'] = $this->card;
			return [$options];
	}
	
	
	
	protected function getStripeCustomer($token = null, array $options = [])
	{
		if (! $this->buyer->stripe_id) {
			$customer = $this->buyer->createAsStripeCustomer($token, $options);
		} else {
			$customer = $this->buyer->asStripeCustomer();
			if ($token) {
				$this->buyer->updateCard($token);
			}
		}
		return $customer;
	}
	
	function createCustomerToken($acct=false) {
		$options = [
				'customer' => $this->customer->id,
		];
		if ($this->card)
			$options['card'] = $this->card;
		$token = StripeToken::create($options, array("stripe_account" => $acct));
		return $token;
	}
	
	function create_vendor_group($transfer_group, $charge_id = null) {
		$options  = [
				'transfer_type' => StripeFacade::getChargeType(),
				'cc_processor' => 'Stripe',
		];
		return ShoppingCart::CreateVendorGroup($options);
	}
	
	function getOrderGroup() {
		return ShoppingCart::getOrderGroup();
	}
	
	// Temporary methods..
	
	/*
	 function getCard($cust, $card) {
	 
	 $card = StripeCard();
	 }
	 
	 function getFakeCard($customer) {
	 $fakeCard = "card_1CF8aSLuAVFeSRbZdwojiDPQ";
	 echo "Get Fake Card\n";
	 return $customer->sources->retrieve($fakeCard);
	 }
	 */
	
	/*
	function handleFail($failType,$message,...$args) {
		$method = 'handle' .studly_case(str_replace('.','_',$failType));
		if (method_exists($this,$method))
			return $this->{$method}($message,...$args);
			return $this->handleNoFailMethod($message,$args);
	}
	
	function handleNoFailMethod($message,$args) {
		return view('owens.stripe.fail',['message'=>$message]);
	}
	*/
		
	function handleStripeChargeFailed($message) {
		return $this->flashBackError("Error! Please Try again. " . $e->getMessage());
	}
	
	/*
	function create_transfer_group_from_charge($charge) {
		return $this->create_vendor_group($charge->transfer_group, $charge->id);
	}
	
	*/
	
	/*
	 
	 function createCustomer($token) {
	 $user = \Auth::user();
	 try {
	 $customer = \Stripe\Customer::create([
	 'email' => $user->email,
	 'source'=>$token,
	 'description' => "Customer Test",
	 ]);
	 } catch (\Exception $e) {
	 return false;
	 }
	 $user->stripe_id = $customer->id;
	 $user->save();
	 //		echo "Customer";
	 //		print_r($customer);
	 return $customer;
	 }
	 */
	/*
	function directCharge() {
		$description = "Cart Checkout: " . date('c');
		if ($this->vendorGroup->vendors->count() == 0) {
			throw new StripeException("No vendor items in cart",0,null,"no.vendors");
			// return $this->handleFail('no.vendors','No vendor items in cart');
		}
		$passed = 0;
		foreach ($this->vendorGroup->vendors as $vendor) {
			//	$result = $this->doDirectCharge($vendor,$description);
			$result = $this->makeCharge($vendor,$description);
			
			if (!($result instanceof \Exception)) {
				$this->createStripeTransaction($result,$this->vendorGroup);
				$passed++;
			} else if ($passed == 0) {	//Don't continue if first on fails.
				throw new StripeException("Charge Failed",0,$result,"stripe.charge.fail");
			}
		}
		//		event(new ChargeSucessfulEvent($result));
		return $this->finishCharge($passed,$this->vendorGroup);
	}
	*/
	
	/*
	  
	 	function destinationCharge() {
			
		$description = "Cart Checkout: " . date('c');
//		$vendorGroup = $this->create_vendor_group($this->getOrderGroup());
		if ($this->vendorGroup->vendors->count() == 0) {
			return $this->handleFail('no.vendors','No items in cart');
		}
//		$customer = $this->getStripeCustomer($token);
//		$card = $this->getCustomerCard($customer,$card_id);
		$passed = 0;
		foreach ($this->vendorGroup->vendors as $vendor) {
			$result = $this->makeCharge($vendor,$description);
			if (!($result instanceof \Exception)) {
				$this->createStripeTransaction($result,$this->vendorGroup);
				$passed++;
			} else if ($passed == 0) {	//Don't continue if first on fails.
				throw new StripeException("Charge Failed",0,$result,"stripe.charge.fail");
			}
		}
		//		event(new ChargeSucessfulEvent($result));
		return $this->finishCharge($passed,$this->vendorGroup);
	}
 
	
	 */
	/*
	 	function doDirectCharge($vendor,$description="") {
		$token = $this->createCustomerToken($vendor->stripe_user_id);
		$metaData = $this->getMetaData();
		$options = [
				"amount" => $vendor->sale_amount,
				"currency" => "usd",
				"source" => $token, // obtained with Stripe.js
				"description" => $description,
				'metadata' => $metaData,
				'application_fee' => $vendor->fee,
		];
		$options2 = array('stripe_account'=>$vendor->stripe_user_id);
		$result = $this->doCharge($options,$options2);
		if (!($result instanceof \Exception)) {
			$vendor->source_transaction = $result->id;
			$vendor->status="success";
		} else
			$vendor->status="fail";
			return $result;
	}

	 
	 */
		
	/*	
		function doDestinatinCharge($vendor, $description) {
			 $options = $this->buildDestinationPayload($vendor,$description);
			 $result = $this->doCharge($options);
			 if (!($result instanceof \Exception)) {
			 $vendor->source_transaction = $result->id;
			 $vendor->status="success";
			 } else
			 $vendor->status="fail";
			 return $result;
		}
	*/	
		
		
}