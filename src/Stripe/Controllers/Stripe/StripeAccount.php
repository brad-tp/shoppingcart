<?php

namespace Owens\Stripe\Controllers\Stripe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StripeConnect;
use App\Models\StripeTransferGroup;
use App\Models\PendingStripeConnect;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Owens\Stripe\Stripe;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class StripeAccount extends StripeController
{
	
	function index() {
		$accts = StripeConnect::all();
		return view('owens.stripe.accounts',['accts'=>$accts]);
	}
	
	function list() {
		$accts = \Stripe\Account::all(['limit'=>'50']);
		return view('owens.stripe.accounts.list',['accts'=>$accts]);
	}
	
	function fetch(Request $request,$acct) {
		$token = $acct;
		try {
			$result = \Stripe\Account::retrieve($acct);
		} catch (\Exception $e) {
			return $this->handleFail('fetch.no.account',$e->getMessage());
		}
		print_r($result);
		
	}
	
	
	function delete($acct) {
		$a = StripeConnect::where('stripe_user_id',$acct)->first();
		if (!$a) 
			return $this->handleFail('account.not.found',"Account not found");
		try {
			$account = \Stripe\Account::retrieve($acct);
			$account->deauthorize(StripeFacade::getClientId());
		} catch (\Exception $e) {
			return $this->handleFail('account.delete.fail',$e->getMessage(),$a);
		}
		session()->flash("flash_info","Disconnected account");
		$a->update(['active'=>0]);
		return redirect()->back();
	}
	
	
	function handleFailGoBack($message) {
		session()->flash('flash_error',$message);
		return redirect()->back();
	}
	
	function handleAccountDeleteFail($message,$account) {
		return $this->handleFailGoBack($message);
	}
	
	/*
	 function handleActivatePendingKeyNotFound(){
	 echo "Key not found";
	 die();
	 }
	 */
	
	
}
