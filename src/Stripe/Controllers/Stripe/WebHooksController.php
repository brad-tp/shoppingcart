<?php

namespace Owens\Stripe\Controllers\Stripe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Stripe\Event as StripeEvent;
use App\Models\StripeTransferGroup;
use Stripe\Transfer as StripeTransfer;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class WebHooksController extends StripeController
{
	
	function getWHKey() {
		return StripeFacade::getWHKey();
	}
    /**
     *  handles webhook requests, successful requests will call the method handle_event_type 
     *  	(i.e. charge.refunded would call handleChargeRefunded($payload)
     *  
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    function webhook(Request $request) {
    	try {
    		$event = \Stripe\Webhook::constructEvent(
    				$request->getContent(),
    				$request->server("HTTP_STRIPE_SIGNATURE"),
    				$this->getWHKey()
    				);
    	/*
    		if ($event->id != "evt_00000000000000") {	// Webhooks Test event can not be retrieved.
    			$eventDetail = StripeEvent::retrieve($event->id);
    		//	Log::info("Event Detail: ", print_r($eventDetail,true));
    	}
    	*/
    	} catch (\Exception $e) {
    		Log::info("Webhook validation error: " . $e->getMessage());
    		return response("Bad Request",400);
    	}
    	return $this->handle( $event);
    }

    /**
     * Converts $payload->type to callable method and invokes if method exists.
     * 
     * @param StripeEvent $payload
     * @return unknown|\Symfony\Component\HttpFoundation\Response
     */
    function handle(StripeEvent $payload) {
    	$method = 'handle'.studly_case(str_replace('.', '_', $payload->type));
    	if (method_exists($this, $method)) {
    		return $this->{$method}($payload);
    	}
    	return $this->missingMethod($payload);
    }
    
    /**
     * Handle calls to missing methods on the controller.
     *
     * @param  array  $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function missingMethod($payload)
    {
    	Log::info("Missing method: " . print_r($payload,true));
    	return response("Did not handle event",200);
    }
    /**
     * Handles charge.refuned
     * 
     * @param unknown $payload
     */
    public function handleChargeRefunded($payload) {
    	Log::info("Handle Charge Refunded: " . print_r($payload,true));
    	return response("Webhook handled",200);
    }
    
    function doReversalFee($payload) {
    	Log::info("Do Revcersal Fee");
    	$reverse_amount = $payload->data->object->amount_reversed/.9;
    	$reversal_fee = $reverse_amount * .10;
    	$metaData = [];
    	
    	$options = [
    			"amount" => $reversal_fee,
    			"currency" => $payload->data->object->currency,
    			"destination" => StripeFacade::getAccount(),
    			"metadata" => $metaData,
    	];
    	//print_r($options);
    	try {
    		$transfer = StripeTransfer::create($options,['stripe_account'=> $payload->data->object->destination]);
    		echo "Transfer\n";
    		print_r($transfer);
    		/*
    		$vendor->status = 'success';
    		$vendor->stripe_id = $transfer->id;
    		$vendor->balance_transaction = $transfer->balance_transaction;
    		*/
    	} catch (\Exception $e) {
    		//$vendor->status = 'failed';
    		Log::info( "Transfer failed " . $e->getMessage());
    	}
    	
    }
    
    function handleTransferReversedX($payload) {
    	Log::info("Handle Transfer Reversed: " . print_r($payload,true));
    	Log::info("Amount reversed: " . $payload->data->object->amount_reversed);
    	Log::info("Order group: " . $payload->data->object->transfer_group);
    	Log::info("Account: " . $payload->data->object->destination);
    	$transferGroup = StripeTransferGroup::where('transfer_group',$payload->data->object->transfer_group)->first();
    	if ($transferGroup)
    		Log::info("Transfer Group: " . print_r($transferGroup->toArray(),true));
    	else 
    		Log::info("No transfer group");
    	$reverse_amount = $payload->data->object->amount_reversed/.9;
    	$this->doReversalFee($payload);
    	
    	
    }
    
}
