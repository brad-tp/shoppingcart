<?php

namespace Owens\Stripe\Controllers\Stripe;

use Illuminate\Http\Request;
use Owens\Stripe\Stripe;
use Stripe\Account as StripeAccount;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class StripeConnectController extends StripeController
{
	protected $user;
	
	function __construct(Stripe $stripe) {
		// Workaround for no access to sessions.
		$this->middleware(function ($request, $next) {
			$this->user= \Auth::user();
			$this->stripeConnectManager = new \Owens\Stripe\StripeConnectManager($this->user);
			return $next($request);
		});			
	}
    //
	function canOnboard() {
		return true;
	}
	
	function index() {
		if (!$this->canOnboard()) {
			return $this->handleFail('onboard.not.allowed',"You are not authorized to accept credit cards");
		}
		return view('owens.stripe.connect',[
				'client_id'=>StripeFacade::getClientId(),
		]);
	}
	
	function onboard(Request $request, Stripe $stripe) {
		if (!$this->canOnboard()) {
			return $this->handleFail('onboard.not.allowed',"You are not authorized to accept credit cards");
		}
		if ($request->error) {
			return $this->handleFail('stripe.onboard.error',$request->error_description);
		}
		$token=$request->code;
		if (($info = $this->stripeConnectManager->handleOnboard($token)) === false)
			return $this->handleFail('stripe.onboard.fail',$this->stripeConnectManager->getErrors()[0]);
		if ($info === true)
			return view("owens.stripe.check_email");
		else
			return view("owens.stripe.onboard");
	}
	
	function activeStripe(Request $request, $key) {
		if (!$this->canOnboard())
			return $this->handleFail('onboard.not.allowed');
		if (!$this->stripeConnectManager->acceptPendingConnect($key))
			return $this->handleFail('activate.fail',$this->stripeConnectManager->getErrors()[0]);
		return view("owens.stripe.onboard");
	}
	
	function handleStripeOnboardError($message) {
		return $this->flashRouteInfo('Onboard error: ' . $message, 'stripe.connect');
	}
	
	function handleStripeOnboardFail($message) {
		return view('owens.stripe.onboard_fail',['reason'=>$message]);
	}
	
	function customAccount() {
		echo "<pre>\n";
		echo "Custom account\n";
		$options = [
				'type'=>'custom',
				'country'=>'US',
				'email' => \Auth::user()->email,
		];
		try {
			$acct = StripeAccount::create($options);
		} catch (\Exception $e) {
			echo "Failed to create account: " . $e->getMessage() . "\n";
			die();
		}
		print_r($acct);
		$acct->access_token = "No Token";
		$acct->refresh_token = "No Token";
		$acct->stripe_publishable_key = "No Key";
		$acct->stripe_user_id = $acct->id;
		$acct->scope = "None";
		return $this->setUpAccountActivation($acct);
	}
	
	/*
	function setUpAccountActivation($result) {
		if (StripeFacade::autoActivate()) {
			$this->activateStripe($result);
			return view("owens.stripe.onboard");
		} else
			$this->sendActivateMail($result);
		return view("owens.stripe.check_email");
	}
	
	public function sendActivateMail($result) {
		$params = array_merge($result->__toArray(),[
				'user_id' => \Auth::user()->id,
				'seller_id' => \Auth::user()->getSellerId(),
				'key' => str_random(60),
		]);
		$pending = PendingStripeConnect::create($params);
		Mail::queue(new \Owens\Stripe\Mail\StripeConnectMail($pending));
	}
	
	
	function activateConnect($options) {
		if (!array_key_exists('seller_id',$options))
			$options['seller_id'] = \Auth::user()->getSellerId();
		if (!array_key_exists('user_id',$options)) 
			$options['user_id'] = \Auth::user()->id;
		$params['active'] = 1;
		$info = StripeConnect::updateOrCreate(['seller_id'=>$options['seller_id']],$options);
		\Auth::user()->setStripeConnect($info);
		return $info;
	}
	
	function activateStripe($result) {
		$params = $result->__toArray();
		$this->activateConnect($params);
	}
	
	function pendingAccept($pending) {
		$params = $pending->toArray();
		$this->activateConnect($params);
		$pending->accepted_at = Carbon::now();
		$pending->save();
	}
	
	*/
	
}
