<?php

namespace Owens\Stripe\Controllers\Stripe;

use Illuminate\Http\Request;
use Stripe\Account;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class StripeDashboardController extends StripeController
{
	/**
	 * Displays Enter dashboard page.
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
	
    function index() {
    	if (!$this->canViewDashboard()) {
    		return $this->handleFail('dashboard.not.allowed',"No access allowed to dashboard");
    	}
    	
    	return view('owens.stripe.dashboard');
    }
    
  	/**
  	 * Returns true if allowed to view dashboard.
  	 * 
  	 * @return boolean
  	 */
    function canViewDashboard() {
    	return true;
    }
    
    /**
     * Determines type of stripe account and redirects them to the appropiate dashboard.
     * 
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    
    function enter() {
    	if (!$this->canViewDashboard()) {
    		return $this->handleFail('dashboard.not.allowed',"No access allowed to dashboard");
    	}
    	$acct = \Auth::user()->getStripeAccount();
    	if (!$acct)
    		return $this->handleFail('dashboard.no.active.account',"You do not have an active connected account with Team Play 1.");
    	StripeFacade::getKey();	// Make stripe instance occur.
    	try {
    		$account = Account::retrieve($acct);
    	} catch (\Excepton $e) {
    		return $this->handleFail('stripe.no.account',$e->getMessage());
    	}
    	if ($account->type == "standard")
    		$url = "https://dashboard.stripe.com";
    	else if ($account->type = "express") {
    		$url = $account->login_links->create()->url;
    	} else {
    		session()->flash('flash_error',"No Dashboard");
    		return redirect()->back();
    	}
    	return redirect($url);
    }
  
}
