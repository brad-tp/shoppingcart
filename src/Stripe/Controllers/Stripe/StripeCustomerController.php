<?php

namespace Owens\Stripe\Controllers\Stripe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Customer as StripeCustomer;

class StripeCustomerController extends StripeController
{
    //
    function index() {
    	try {
    	$customers = StripeCustomer::all(['limit' => 30]);
    	} catch(\Exception $e) {
    		echo $e->getMessage();
    		die();
    	}
    	return view('owens.stripe.customers.index',[
    			'customers' => $customers,
    	]);
    }
    
    function delete($id) {
    	try {
    		$customer = StripeCustomer::retrieve($id);
    		$customer->delete();
    	} catch(\Exception $e) {
    		return $this->flashBackError($e->getMessage());
    	}
    	return $this->flashBackInfo("Customer Deleted: $id");
    }
}
