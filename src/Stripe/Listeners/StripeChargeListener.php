<?php

namespace Owens\Stripe\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\Models\VendorGroup;
use Owens\Stripe\Stripe;
use App\Models\VendorChargeGroup;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class StripeChargeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Stripe $stripe)
    {
        //
        $this->stripe = $stripe;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        Log::debug("Charge Event occured: " . print_r($event->charge,true));
        if (StripeFacade::isMultiTransferMode()) {
      //  	echo "Transfer group is: " . $event->charge->transfer_group . "\n";
        	$transferGroup = config('shoppingcart.models.vendor_charge_group')::with('vendors')->where('transfer_group',$event->charge->transfer_group)->first();
     //   	print_r($transferGroup->toArray());
        	Log::debug("Transfer Group: " . print_r($transferGroup->toArray(),true));
       		$transferGroup->doTransfer();
        }
    }
}
