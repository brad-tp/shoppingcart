<?php

namespace Owens\Stripe;

use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Owens\Stripe\Facade\Stripe as StripeFacade;


class StripeConnectManager {
	protected $owner;
	protected $errors = [];
	
	function __construct($owner) {
		$this->owner = $owner;
	//	$this->stripe = resolve('Owens\Stripe\Stripe');
	}
	
	function ReflectionCreate($class, ...$args) {
		return (new \ReflectionClass($class))->newInstance(...$args);
	}
	
	function getErrors() {
		return $this->errors;
	}
	
	function errorOut($msg) {
		$this->errors[] = $msg;
		return false;
	}
	
	/*
	function getPendingStripeConnectModel() {
		return '\App\Models\PendingStripeConnect';
	}
	
	function getStripeConnectModel() {
		return '\App\Models\StripeConnect';
	}
	*/
	public function sendActivateMail($result) {
		$params = array_merge($result->__toArray(),[
				'user_id' => \Auth::user()->id,
				'seller_id' => \Auth::user()->getSellerId(),
				'key' => str_random(60),
		]);
//		$pending = PendingStripeConnect::create($params);
//		$pending = $this->ReflectionCreate($this->getPendingStripeConnectModel(),$params);
//		$pending->save();
//		$pending = $this->stripe->getPendingStripeConnectModel()::create($params);
		$pending = StripeFacade::getPendingStripeConnectModel()::create($params);
		
		Mail::queue(new \Owens\Stripe\Mail\StripeConnectMail($pending));
		return true;
	}
		
	function setUpAccountActivation($result) {
		if (StripeFacade::autoActivate()) {
			return $this->activateStripe($result);
			return view("owens.stripe.onboard");
		} else
			return $this->sendActivateMail($result);
			return view("owens.stripe.check_email");
	}
	
	function handleOnBoard($token) {
		try {
			$result = \Stripe\OAuth::token([
					'code'=>$token,
					'grant_type'=>'authorization_code'
			]);
		} catch (\Exception $e) {
			return $this->errorOut($e->getMessage());
		}
		return $this->setUpAccountActivation($result);
	}
	
	function activateConnect($options) {
		if (!array_key_exists('seller_id',$options))
			$options['seller_id'] = $this->owner->getSellerId();
		if (!array_key_exists('user_id',$options))
			$options['user_id'] = $this->owner->id;
		$options['active'] = 1;
//		$info = StripeConnect::updateOrCreate(['seller_id'=>$options['seller_id']],$options);
		$info =StripeFacade::getStripeConnectModel()::updateOrCreate(['seller_id'=>$options['seller_id']],$options);
		
		$this->owner->setStripeConnect($info);
		return $info;
	}
	
	function activateStripe($result) {
		$params = $result->__toArray();
		return $this->activateConnect($params);
	}
	
	function acceptPendingConnect($key) {
//		$pending = PendingStripeConnect::where('key',$key)->first();
		$pending = StripeFacade::getPendingStripeConnectModel()::where('key',$key)->first();
		
		if (!$pending)
			return $this->errorOut("Key not found");
		if ($pending->user_id != \Auth::user()->id)
			return $this->errorOut("You did not request this credit setup: ");
		if ($pending->accepted_at != null)
			return $this->errorOut("You already accepted this request",$pending);
		$this->pendingAccept($pending);
		return true;
	}
	
	function pendingAccept($pending) {
		$params = $pending->toArray();
		$this->activateConnect($params);
		$pending->accepted_at = Carbon::now();
		$pending->save();
	}
	
	
}