<?php

namespace Owens\Stripe\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class StripeConnectMail extends Mailable
{
    use Queueable, SerializesModels;

    public $pending;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pending)
    {
        //
        $this->pending = $pending;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	return $this
    		->subject('Complete your TeamPlay1 credit setup')
    		->to($this->pending->user->email)
    		->markdown('owens.stripe.mail.stripe_connect');
    }
}
