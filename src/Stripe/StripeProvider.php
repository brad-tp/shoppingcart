<?php

namespace Owens\Stripe;

// use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class StripeProvider extends ServiceProvider
{
	protected $namespace = "Owens\Stripe\Controllers";
	
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    //    Route::group(['namespace'=>"Owens\Stripe\Controllers"], __DIR__ ."/routes/stripeweb.php");
    	parent::boot();
    	
    	Event::listen('Owens\Stripe\Events\ChargeSucessfulEvent', 'Owens\Stripe\Listeners\StripeChargeListener');
    	$this->publishes([__DIR__ . '/../stubs/Controllers/Stripe' => app_path('/Http/Controllers/Stripe')], 'controllers');
    	$this->publishes([__DIR__ . '/../public/js' => public_path('/js')], 'javascript');
    	$this->publishes([__DIR__ . '/../resources/views/owens/stripe' => resource_path('/views/owens/stripe')], 'views');
    	
    }

    protected function mapStripeRoutes()
    {
    	Route::middleware('web')
    	->namespace($this->namespace)
    	->group(__DIR__ .'/routes/stripeweb.php');
    }
    
    function map() {
    	$this->mapStripeRoutes();
    }
    
    public function loadAliases() {
    	$loader = \Illuminate\Foundation\AliasLoader::getInstance();
    	$loader->alias('Stripe', \Owens\Stripe\Facade\Stripe::class);
    }
    
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    	$this->app->register(\Laravel\Cashier\CashierServiceProvider::class);
    	
        $this->app->singleton('Owens\Stripe\Stripe',function() {
        	return new Stripe(Config::get('shoppingcart.stripe'));
        });
        $this->loadAliases();
    }
}
