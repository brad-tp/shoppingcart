<?php 
namespace Owens\Stripe\Exceptions;

use Exception;

class StripeException extends Exception {
	protected $label = "generic";
	
	function __construct($msg,$code = 0,$previous = null, $label=false) {
		parent::__construct($msg,$code,$previous);
		if ($label)
			$this->label = $label;
	}
	
	function getLabel() { return $this->label; }
}