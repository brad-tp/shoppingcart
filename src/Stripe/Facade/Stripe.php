<?php 

namespace Owens\Stripe\Facade;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Route;

class Stripe extends Facade {
	
	protected static function  getFacadeAccessor() {
		return "Owens\Stripe\Stripe";
	}
	
//	static function routes() {
//		self::myroutes();
//	}
}
