<?php
namespace Owens\Stripe;

use Illuminate\Support\Facades\Route;

class Stripe {
	
	const VENDOR_MODE_DIRECT = 1;
	const VENDOR_MODE_MULTITRANSFER = 2;
	const VENDOR_MODE_DESTINATION = 3;
	const VENDOR_MODE_STANDALONE = 4;
	
	private $vendorModes = [
		self::VENDOR_MODE_DIRECT => 'direct',
		self::VENDOR_MODE_MULTITRANSFER => 'multitransfer',
		self::VENDOR_MODE_DESTINATION => 'destination',
		self::VENDOR_MODE_STANDALONE => 'standalone',
			
	];
	
	protected $key;
	protected $secret;
	protected $stripe;
	protected $wh_key;
	protected $client_id;
	protected $auto_activate = false;
	protected $fee_rate = 0.10;
	protected $transaction_fee = 0;
	protected $currency = "usd";
	protected $account = false;	// Just stubbed for now, need to get from config
	protected $vendor_mode = 'multitransfer';
	
	function __construct($config) {
		foreach ($config as $key=>$value) {
			$this->{$key} = $value;
		}
		if (array_key_exists('vendor_mode',$config)) {	// Make sure it is a valid mode.
			if (!$this->findVendorMode($config['vendor_mode']))
				throw new \StripeException('Invalid vendor mode');
		}
		$this->setApiKey();
	}
	
	function setApiKey() {
		\Stripe\Stripe::setApiKey ( $this->getSecret());
	}
	
	function getKey() {
		return $this->key;
	}
	
	function getSecret() {
		return $this->secret;
	}
	
	function getWHKey() {
		return $this->wh_key;
	}
	
	function getClientId() {
		return $this->client_id;
	}
	
	function autoActivate() {
		return $this->auto_activate;
	}
	
	function getFeeRate() {
		return $this->fee_rate;
	}
	
	function setFeeRate($rate) {
		return $this->fee_rate = $rate;
	}
	
	function getFee($amount) {
		\Log::info("Get fee transaction_fee is: " . $this->transaction_fee);
		return intval(round($amount * $this->fee_rate) + $this->transaction_fee);
	}
	function setTransactionFee($amount) {
		$this->transaction_fee = $amount;
	}
	
	function getCurrency() {
		return $this->currency;
	}
	
	function getAccount() {
		return $this->account;
	}
	
	function isDirectCharge() {
		return ($this->vendor_mode == "direct");
	}
	
	function isMultiTransferMode() {
		return ($this->vendor_mode == "multitransfer");
	}
	
	function isDestinationCharge() {
		return ($this->vendor_mode == "destination");
	}
	
	function isStandAlone() {
		return ($this->vendor_mode == "standalone");
	}
	
	function getChargeType() {
		return $this->vendor_mode;
	}
	
	function getStripeConnectModel() {
		return '\Owens\ShoppingCart\Models\StripeConnect';
	}
	
	function getPendingStripeConnectModel() {
		return '\Owens\ShoppingCart\Models\PendingStripeConnect';
	}
	
	function getStripeTransactionModel() {
		return '\Owens\ShoppingCart\Models\StripeTransaction';
	}
	
	function findVendorMode($mode) {
		return array_search(strtolower($mode),$this->vendorModes);
	}	
	
	public static function accountAdminRoutes(array $options=[]) {
		if (!array_key_exists('prefix',$options))
			$options['prefix'] = '/stripe';
		Route::group($options,function($router) {
			$router->get('/dashboard','Stripe\StripeDashboardController@index')->name('stripe.dashboard');
			$router->get("/dashboard/enter",'Stripe\StripeDashboardController@enter')->name('stripe.dashboard.enter');
			
			Route::prefix('/admin')->group(function($router) {
				$router->get('/accounts','Stripe\StripeAccount@index')->name('stripe.accounts');
				$router->get('/accounts/fetch/{stripe_acct}','Stripe\StripeAccount@fetch')->name('stripe.accounts.fetch');
				$router->get('/accounts/list','Stripe\StripeAccount@list')->name('stripe.accounts.list');
				$router->get('/accounts/delete/{acct}','Stripe\StripeAccount@delete')->name('stripe.accounts.delete');
			});
			Route::prefix('/connect')->group(function($router) {
				$router->get('/','Stripe\StripeConnectController@index')->name('stripe.connect');
				$router->get('/onboard','Stripe\StripeConnectController@onboard')->name('stripe.connect.onboard');
				$router->get('/activate/{key}','Stripe\StripeConnectController@activeStripe')->name('stripe.connect.activate');
				$router->get('/connect/custom','Stripe\StripeConnectController@customAccount')->name('stripe.connect.custom');
			});
			Route::prefix('/customers')->group(function($router) {
				$router->get('/','Stripe\StripeCustomerController@index')->name('stripe.customers');
				$router->get('/delete/{id}','Stripe\StripeCustomerController@delete')->name('stripe.customers.delete');
				
			});
		});
	}
	
	public static  function privateRoutes(array $options=[]) {
		if (!array_key_exists('prefix',$options))
			$options['prefix'] = '/stripe';
		Route::group($options,function($router) {
			$router->get('/','Stripe\StripeChargeController@index')->name("stripe");
			$router->post('/','Stripe\StripeChargeController@post')->name("stripe");
		});	
	}

	
	public static function publicRoutes(array $options = []) {
		if (!array_key_exists('prefix',$options))
			$options['prefix'] = '/stripe';
		return Route::group($options,function($router) {
			$router->get("/transfergroup/{id}",function($id) {
				echo "<PRE>";
				echo "TransferGroup test";
				$transferGroup = App\Models\StripeTransferGroup::find($id);
				$transferGroup->load('vendors');
				print_r($transferGroup->toArray());
				print_r($transferGroup->vendors[0]->toArray());
				$owner = $transferGroup->vendors->where('owner_id',74)->first();
				print_r($owner->toArray());
				$owner->transfer_amount = 500;
				print_r($owner->toArray());
			});
			$router->get('/webhook','Stripe\WebHooksController@webhook')->name('stripe.webhook');
			$router->post('/webhook','Stripe\WebHooksController@webhook')->name('stripe.webhook');
		});
	}
}