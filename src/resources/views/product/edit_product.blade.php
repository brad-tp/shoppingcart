@extends(config('shoppingcart.layouts.default'))

@section('content')

@component('page.components.panel2',[
	'panel_heading'=> '<h2 class="text-center"> Edit Product </h2>',
])				

	<div class="row" id="productForm">


@include('product.ProductForm')

</div>
@endcomponent

@if (config('teamplay1.system.debug'))
<PRE>
<?php print_r(session()->getOldInput()) ?>
<?php print_r($product->toArray()) ?>
</PRE>
@endif

@endsection