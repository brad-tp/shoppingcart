@extends(config('shoppingcart.layouts.default'))

@section('content')

<h2> Product List </h2>

<table class="table table-striped">
<thead>
<tr class="info">
<th> Name </th>
<th> Type </th>
<th> Description </th>
<th> Start </th>
<th> End </th>
<th> Signup Deadline </th>
<th></th>


</tr>
</thead>
<tbody>
@foreach ($products as $product)
	<tr>
		<td> {{ $product->name }} </td>
		<td> {{ $product->ProductType->name }} </td>
		<td> {{ $product->description }} </td>
		<td> @if ($product->start) {{ $product->start->format('Y-m-d') }} @endif </td>
		<td> @if ($product->end)  {{ $product->end->format('Y-m-d') }} @endif </td>
		<td> @if ($product->signup_expires) {{ $product->signup_expires->format('Y-m-d') }} @endif </td>
	    <td> <a href="{{ route('product.edit',[
	    			'product' => $product->id
	    		]) }}"> 
	    		<button class="btn btn-info" type="button"> Edit </button>
	    	</a> 
	    </td>
	</tr>

@endforeach

</tbody>
</table>
<a href="{{ route('product.new')}}"> New Product </a>
<br>

@if (config('teamplay1.system.debug'))
<pre>
<?php print_r($products->toArray()); ?>
</pre>
@endif

@endsection