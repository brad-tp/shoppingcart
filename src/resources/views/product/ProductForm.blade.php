
	<form method="POST" >
		<div class="form-group @if ($errors->has('name')) has-error @endif">
			<label> Product Name </label>
			<input class="form-control" type="text" name="name" value="{{ old('name') }}">
					@if ($errors->has('name'))
						<span class="help-block">
						<strong>{{ $errors->first('name') }}</strong>
						</span>
					@endif
		</div>
		<div class="form-group @if ($errors->has('product_type_id')) has-error @endif">
		<label> Product Type: </label>
		<select class="form-control" name="product_type_id">
			<option > No Selection </option>
			@foreach ($productTypes as $productType)
				<option value="{{ $productType->id }}"
					@if (old('product_type_id') == $productType->id) selected @endif
				> 
				{{ $productType->name }} </option>
			@endforeach
		</select>
		</div>
		<div class="form-group @if ($errors->has('sport')) has-error @endif">
			<label> Sport: </label>
			@include('include.partials.select_sport_list',[
			'name'=>'sport',
			'class'=>'form-control',
			'selected'=>old('sport')
			])
		</div>
		<div class="form-group @if ($errors->has('description')) has-error @endif">
			<label> Description </label>
			<textarea class="form-control" name="description">{{ old('description') }}</textarea>
					@if ($errors->has('description'))
						<span class="help-block">
						<strong>{{ $errors->first('description') }}</strong>
						</span>
					@endif
		</div>
		
		<div class="form-group @if ($errors->has('start')) has-error @endif">
			<label> Start Date  </label>
			<input class="form-control" type="date" name="start" placeholder = "MM-DD-YYYY" value="{{ (old('start')?(new \DateTime(old('start')))->format('Y-m-d'):"") }}">
					@if ($errors->has('start'))
						<span class="help-block">
						<strong>{{ $errors->first('start') }}</strong>
						</span>
					@endif
		</div>
		<div class="form-group @if ($errors->has('end')) has-error @endif">
			<label> End Date </label>
			<input class="form-control" type="date" name="end" placeholder = "MM-DD-YYYY" value="{{ (old('end')?(new \DateTime(old('end')))->format('Y-m-d'):"") }}">
					@if ($errors->has('end'))
						<span class="help-block">
						<strong>{{ $errors->first('end') }}</strong>
						</span>
					@endif
		</div>
		<div class="form-group @if ($errors->has('signup_expires')) has-error @endif">
			<label> Signup Expires </label>
			<input class="form-control" type="date" name="signup_expires" placeholder = "MM-DD-YYYY" value="{{ (old('signup_expires')?(new \DateTime(old('signup_expires')))->format('Y-m-d'):"") }}">
					@if ($errors->has('signup_expires'))
						<span class="help-block">
						<strong>{{ $errors->first('signup_expires') }}</strong>
						</span>
					@endif
		</div>
		
		<div class="form-group @if ($errors->has('price')) has-error @endif">
			<label> Price </label>
			<input class="form-control" type="text" placeholder="0.00" name="price" value="{{ old('price') }}">
					@if ($errors->has('price'))
						<span class="help-block">
						<strong>{{ $errors->first('price') }}</strong>
						</span>
					@endif
		</div>
		<div class="text-center">
			<button class="btn btn-info" type="submit"> Submit </button>
			<button class="btn btn-info" type="reset"> Clear </button>
		</div>
		{{ csrf_field() }}
		
	</form>
	
