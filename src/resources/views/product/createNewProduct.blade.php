@extends(config('shoppingcart.layouts.default'))

@section('content')

@component('page.components.panel2',[
	'panel_heading'=> '<h2 class="text-center"> New Product </h2>',
])				

	<div class="row" id="productForm">
	@include('product.ProductForm')
	
	</div>
@endcomponent
@endsection