@extends(config('shoppingcart.layouts.default'))

@section('content')
<h2>
Stripe
</h2>

<form method="Post" id="subscription-form" action="{{route('stripe')}}">
@if (isset($hiddens))
	@forelse($hiddens as $key=>$value) 
		<input type="hidden" name="{{$key}}" value="{{$value}}">
	@empty
	@endforelse
@endif

@if (\Auth::user() && \Auth::user()->stripe_id)
	<input name="use_card" type="checkbox" value=2> Use Exising card?
	<h4> Cards </h4>
	@foreach (\Auth::user()->cards() as $card)
		<input type="radio" name="card" value={{ $card->id }}> {{ $card->brand }} xxxx xxxx xxxx {{ $card->last4 }} {{$card->exp_month}}/{{$card->exp_year}} <br>
	@endforeach
		<input type="radio" name="card"> Other card
@endif
	  <div class="form-row">
    <label for="card-element">
      Credit or debit card
    </label>
    <div data-strip-card=true data-key='{{ $api_key }}' id="card-element" class="form-control">
      <!-- a Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display form errors -->
    <div id="card-errors" role="alert"></div>
  </div>

  <input type="submit" class="submit" value="Submit Payment">
{{ csrf_field() }}
</form>

<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="/js/stripe.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>

<br><br>
<pre>
@php
$user = \Auth::user();
if ($user && $user->stripe_id) {
	if ($user->cards()) {
		echo "has cards\n";
		print_r($user->cards()->toArray());
		echo "default card";
		$default = $user->defaultCard();
		print_r($default);
	}
}
@endphp

</pre>

@endsection