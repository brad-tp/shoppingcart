@extends(config('shoppingcart.layouts.default'))

@section('content')
<h2> Stripe Failure </h2>

Stripe failed due to: {{ $message }}

@endsection