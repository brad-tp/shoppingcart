@extends(config('shoppingcart.layouts.default'))

@section('content')

<h2> Your account failed to be set up </h2>
{{ $reason }}
@endsection
