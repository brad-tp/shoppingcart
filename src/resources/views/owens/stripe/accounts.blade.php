@extends(config('shoppingcart.layouts.default'))

@section('content')
<h2> Accounts </h2>
<table class="table">
@foreach ($accts as $acct) 
<tr>
	<td> {{ $acct-> id }} </td> 
	<td> {{ $acct->access_token }} </td> 
	<td> {{ $acct->stripe_publishable_key }} </td> 
	<td> <a href="{{ route('stripe.accounts.fetch',['acct'=>$acct->stripe_user_id]) }}"> {{ $acct->stripe_user_id }} </a> </td> 
	<td> {{ $acct->stripe_scope }} </td>
</tr>	
@endforeach

</table>
<pre>
@php
print_r($accts->toArray());
@endphp
</pre>
@endsection