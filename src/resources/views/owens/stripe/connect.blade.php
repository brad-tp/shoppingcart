@extends(config('shoppingcart.layouts.default'))

@section('content')
<h2> Stripe Connect </h2>

<a class="btn btn-info" 
href="https://connect.stripe.com/express/oauth/authorize?redirect_uri={{route('stripe.connect.onboard')}}&client_id={{ $client_id }}&state=1234"> Connect with Stripe </a>

<a class="btn btn-info" 
href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id={{ $client_id }}&scope=read_write&redirect_uri={{route('stripe.connect.onboard')}}"> 
 Standard Connect with Stripe </a>

<a class="btn btn-info" href="{{route('stripe.connect.custom') }}"> Custom Account </a>

@endsection


{{--
http://98.156.1.161:8083/paypal/stripe/onboard&client_id={{ $client_id }}&state=1234"> Connect with Stripe </a>

<a class="btn btn-info" 
href="https://connect.stripe.com/express/oauth/authorize?redirect_uri={{ route('stripe.account.onboard',[
	'client_id'=>'ca_CHwRI38qQYXiBlkZhtCaSOV4oMAxCUXM',
	'state=1234'
	]) }}"> Connect with Stripe </a>

<a class="btn btn-info" 
href="https://connect.stripe.com/express/oauth/authorize?redirect_uri={{ route('stripe.account.onboard',[
	'client_id'=>'ca_CHwRI38qQYXiBlkZhtCaSOV4oMAxCUXM',
	'state=1234'
	]) }}"> Standard Connect with Stripe </a>

--}}