@extends(config('shoppingcart.layouts.default'))

@section('content')
<h2>
Stripe
</h2>

<form method="Post" id="subscription-form">
<input type="hidden" value="300" name="amount" ">
{{--
	@include ('owens.stripe.partials.card_info')
	<button class="btn btn-info" id="customButton"> Make Payment </button>
--}}

{{--
	  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_laY4IxLiWBMfbv3TjTMNEi2w"
    data-amount="999"
    data-name="Teamplay1"
    data-description="Widget"
    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
    data-locale="auto">
  </script>
--}}
	  <div class="form-row">
    <label for="card-element">
      Credit or debit card
    </label>
    <div data-strip-card=true data-key='pk_test_laY4IxLiWBMfbv3TjTMNEi2w' id="card-element">
      <!-- a Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display form errors -->
    <div id="card-errors" role="alert"></div>
  </div>

  <input type="submit" class="submit" value="Submit Payment">
{{ csrf_field() }}
</form>

<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="js/stripe.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>

{{--
<script>
$('document').ready(function() {
	var handler = StripeCheckout.configure({
	  key: 'pk_test_laY4IxLiWBMfbv3TjTMNEi2w',
	  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
	  locale: 'auto',
	  token: function(token) {
	    // You can access the token ID with `token.id`.
	    // Get the token ID to your server-side code for use.
	  }
	});
	
	document.getElementById('customButton').addEventListener('click', function(e) {
	  // Open Checkout with further options:
	  handler.open({
	    name: 'Teamplay1',
	    description: '2 widgets',
	    amount: 2000
	  });
	  e.preventDefault();
	});
	
	// Close Checkout on page navigation:
	window.addEventListener('popstate', function() {
	  handler.close();
	});
});

</script>
--}}

<script>
$('document').ready(function() {
	/*
	var stripe = Stripe('pk_test_laY4IxLiWBMfbv3TjTMNEi2w');
	var elements = stripe.elements();

	var card = elements.create('card');

	// Add an instance of the card UI component into the `card-element` <div>
	card.mount('#card-element');
*/

/*
	function stripeTokenHandler(token) {
		  // Insert the token ID into the form so it gets submitted to the server
		  var form = document.getElementById('subscription-form');
		  var hiddenInput = document.createElement('input');
		  hiddenInput.setAttribute('type', 'hidden');
		  hiddenInput.setAttribute('name', 'stripeToken');
		  hiddenInput.setAttribute('value', token.id);
		  form.appendChild(hiddenInput);

		  // Submit the form
		  form.submit();
		}

		function createToken() {
		  stripe.createToken(card).then(function(result) {
		    if (result.error) {
		      // Inform the user if there was an error
		      var errorElement = document.getElementById('card-errors');
		      errorElement.textContent = result.error.message;
		    } else {
		      // Send the token to your server
		      stripeTokenHandler(result.token);
		    }
		  });
		};

		// Create a token when the form is submitted.
		var form = document.getElementById('subscription-form');
		form.addEventListener('submit', function(e) {
		  e.preventDefault();
		  createToken();
		});

	*/
});



</script>

@endsection