@extends(config('shoppingcart.layouts.default'))

@section('content')
<h2> Check you email </h2>

Thank you for setting up ecommerce with Team Play 1.  Please check your email to complete the process.

@endsection