@component('mail::message')

@component('mail::panel')

You have requested to register for selling goods and services on Teamplay1.com.
	
Complete your credit setup at Teamplay1.com
	@component('mail::button',[
			'url'=>route('stripe.connect.activate',[
				'key'=>$pending->key
			]),
			'color'=>'green',
		])
			Complete Credit
	@endcomponent
	This activation link is only good for 24 hours.	
@endcomponent
@endcomponent
