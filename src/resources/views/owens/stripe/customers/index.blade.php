@extends(config('shoppingcart.layouts.default'))

@section('content')
<h2> Stripe Customers </h2>
<table class="table table-striped">
@foreach ($customers->data as $customer)
<tr>
<td> {{ $customer->email }} </td>
<td> {{ $customer->id }} </td>
<td> {{ $customer->description }} </td>
<td> {{ $customer->default_source }} </td>

<td> <a href="{{ route('stripe.customers.delete', ['id' => $customer->id]) }}" class="btn btn-xs btn-danger"> Delete </a> </td>
</tr>
@endforeach

</table>

<pre>
@php
print_r($customers);
@endphp
</pre>
@endsection