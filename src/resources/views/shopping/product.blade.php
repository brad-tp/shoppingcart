@extends(config('shoppingcart.layouts.default'))


@section('content')
<h2> Shopping Product </h2>
	<div class="row">
		<div class="col-sm-2"> 
			<a href="{{ route('shopping.product',[
							'id' => $product->id
					]) }}"> 
					{{ $product->name }}
			</a>
		</div>
		<div class="col-sm-2">
			@if ($product->start && $product->end)
				{{ $product->start->format('Y-m-d') }}  -  {{ $product->end->format('Y-m-d') }}
			@endif
		</div>
		<div class="col-sm-5">
			{{ $product->description }}
		</div>
		{{-- Require Member Select?? --}}
		<div class="col-sm-3"> 
		@include('include.partials.product_to_cart',['product'=>$product])
		</div>
	</div>
<br>
<pre>
<?php print_r($product->toArray()) ?>
</pre>

@endsection