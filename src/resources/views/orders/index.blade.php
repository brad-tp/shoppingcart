@extends(config('shoppingcart.layouts.default'))

@section('content')
<h3> Invoices </h3>
<hr>
<table class="table table-striped table-responsive">
<thead>
<th> Invoice ID </th>
<th> Date </th>
<th> Items </th>
<th> Total </th>
</thead>
<tbody>
@foreach($invoices as $invoice) 
<tr>
	<td> <a href="{{ route('invoice.show',['id'=>$invoice->id])}}"> {{ $invoice->id }} </a> </td>
	<td> {{ $invoice->created_at }} </td>
	<td> {{ $invoice->Items->count() }} </td>
	<td> {{ $invoice->total }} </td>
	
</tr>
@endforeach
</tbody>
</table>


@if (config('teamplay1.system.debug'))
<br><br>
<pre>
<?php 
if ($invoices) {
	print_r($invoices->toArray());
}
?>
</pre>

@endif

@endsection