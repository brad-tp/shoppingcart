@extends(config('shoppingcart.layouts.default'))

@section('content')

<style>
	table#invoice > tbody > tr > td {
		border: none;
	}
	
	
</style>

<h2> Invoice #{{ $invoice->id }} </h2>
<table id="invoice" class="table invoice" >
<tbody>
	<tr> <td></td><td></td></tr>
	@if ($invoice->billing_address)
	<tr>
		<td style="color: purple;">
			@include('invoice.address',['label'=>'Billing Address','address'=>$invoice->billing_address])
		</td>
		<td>
			@include('invoice.address',['label'=>'Shipping Address','address'=>$invoice->shipping_address])
			<br><br>
		</td>
	</tr>
	@endif
	<tr> <td> </td> <td></td></tr>
	<tr>
		<td colspan=2>
			<table class="table table-responsive table-hover">
			<thead>
			<th class="text-left"> ID </th>
			<th class="text-left"> Name </th>
			<th class="text-left">From</th>
			<th class="text-left"> Description </th>
			<th class="text-center"> QTY </th>
			<th class="text-center"> Price </th>
			<th class="text-center"> Total </th>
			</thead>
			<tbody>
			@foreach ($invoice->Items as $item) 
			<tr>
				<td> {{ $item->id }} </td>
				<td> {{ $item->product_name }} </td>
				<td> {{ $item->seller->name }} </td>
				<td> {{ $item->description }} </td>
				<td  class="text-right"> {{ $item->qty }} </td>
				<td class="text-right"> {{ number_format($item->price,2) }} </td>
				<td class="text-right"> {{ number_format($item->qty * $item->price,2) }} </td>
			</tr>
			@endforeach
			<tr>
				<td colspan=4> </td>
				<th colspan=2> Subtotal </th>
				<th class="text-right"> ${{ number_format($invoice->subtotal,2) }} </th>
			</tr>
			<tr>
				<td colspan=4> </td>
				<th colspan=2> Tax </th>
				<th class="text-right"> ${{ number_format($invoice->tax,2) }} </th>
			</tr>
			<tr>
				<td colspan=4> </td>
				<th colspan=2> Fees </th>
				<th class="text-right"> ${{ number_format($invoice->fee,2) }} </th>
			</tr>
			
			<tr>
				<td colspan=4> </td>
				<th colspan=2> Shipping </th>
				<th class="text-right"> ${{ number_format($invoice->shipping,2) }} </th>
			</tr>
			<tr>
				<td colspan=4> </td>
				<th colspan=2> Total </th>
				<th class="text-right"> ${{ number_format($invoice->total,2) }} </th>
			</tr>
			
			@foreach ($invoice->charges as $charge) 
			<tr>
				<td colspan=4> </td>
				<th colspan=2> Paid </th>
				<th class="text-right"> ${{ number_format($charge->sale_amount/100,2) }} </th>
			</tr>
			@endforeach
			
			
			</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>

@if (config('teamplay1.system.debug') ||  true) 
<br>
<pre>
<?php 	
	print_r($invoice->toArray());
	print_r($invoice->buyer->toArray());
	print_r($invoice->sellers->toArray());
	
	
?>
</pre>
@endif

@endsection