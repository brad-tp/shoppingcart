@php
	$user = \Auth::user();
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--  Style Sheets -->

@if ($user)
	@if ($user->Org && $user->Org->Settings && $user->Org->Settings->Theme)
		<link rel="stylesheet" href="/css/themes/3.3.7/{{ $user->Org->Settings->Theme->dir }}/bootstrap.min.css"/>
	@else
		<link rel="stylesheet" href="/css/themes/3.3.7/cerulean/bootstrap.min.css"/>
	@endif
@else
	<link rel="stylesheet" href="/css/themes/3.3.7/cerulean/bootstrap.min.css"/>
@endif
<link rel="stylesheet" href="/css/TruAlthlete.css"/>
<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css" >

{{--
<link href='/css/fullcalendar.css' rel='stylesheet' />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="/css/comboBox.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
--}}

<script src="/js/jquery-3.2.0.min.js" ></script>
<script src="/js/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

{{--
 <script src="/js/calendarmanager.js" ></script>

<script src='/js/jquery-ui/jquery-ui.min.js'></script>

<script src='/js/teamplay1.js'></script>



<script src='/js/moment.min.js'></script>
<script src='/js/fullcalendar.min.js'></script>
 <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
 
 <script src="/js/comboBox-2017-05-15.js" ></script>
<script src="/js/comboBox.js" ></script>
 <script src="/js/mymap.js"
		></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDorFlt2Ghc2YWMsu-hGBCSoLyr10fTx1Y&libraries=places"
		></script>

--}}
 <!-- 
  {{-- Google Adsense code --}}
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6702696296930690",
    enable_page_level_ads: true
  });
</script>
{{-- End Google Adsense code --}}
-->

</head>
<body>
@include('layouts.menu.topNavBar-testbed')

<div class="container" >
	<div class="row">
		<div style="height:100%;" class="col-sm-10" >
		
	@include('include.flash')
        @yield('content')
		<br><br><br><br>
		</div>
		<div class="col-sm-2">
		</div>
	</div>
	<hr>	
</div>


<div class="footer navbar navbar-fixed-bottom" style="background-color: #f5f5f5;">
	@include('layouts.footer')
</div>



</body>
</html>
		