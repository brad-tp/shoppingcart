
	<nav id="topNav" class="navbar navbar-default">
	<div class="container-fluid" id="topMenu">
		<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
          	<a class="navbar-brand" href="@if ($user && $user->Org) {{route('org_home',['org_nickname' => $user->Org->nickname])}} @endif">
          				@if (($user=Auth::user()) && $user->Org && $user->Org->Settings && $user->Org->Settings->logo_image)		
          					<img height="100%" src="/storage/orgs/{{ $user->org_id }}/images/{{ $user->Org->Settings->logo_image }}">
          				@else
          					<img height="100%" src="/images/teamplay1_logo.png">
          				@endif
          	</a> 
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" id="topMenuItems">
					<li> 	
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" href=""> Manage  
						<span class="caret"></span></a>
						<ul class="dropdown-menu multi-level" role="menu">
						{{--
							<li> <a href="{{route('ad.my.list')}}"> Advertising </a> </li>
							<li> <a href="{{route('scoreboards')}}"> Scoreboards </a> </li>
							<li> <a href="{{ route('tournament.my') }}"> My Tournaments </a> </li>
							<li> <a href="{{ route('tournament.myTeams') }}"> My Tournament Teams </a> </li>
							
							<li> <a href="{{route('resource.list')}}"> Resources </a> </li>
							<li> <a href="{{ route('team') }}">Teams </a> </li>			
							<li> <a href="{{ route('player.numbers.show') }}"> Player Numbers </a> </li>
								--}}
							<li> <a href="{{ route('product.list')}}"> Products </a> </li>
								
						</ul>
					</li>	
					
					<li> 	
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#"> Shopping  
						<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li> <a href="{{route('shopping.products')}}"> Products </a> </li>
							<li> <a href="{{route('shopping.advertising')}}"> Advertising </a> </li>
						{{--	
							
							<li> <a href="{{route('registration.tournament')}}"> Tournament </a> </li>
						--}}							
						</ul>
					</li>
					{{--
					<li> 	
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#"> Tournament Builder
						<span class="caret"></span></a>
						<ul class="dropdown-menu multi-level" role="menu">
							<li> <a href="{{ route('tournament.new') }}"> New Tournament </a> </li>
							<li> <a href="{{ route('tournament.list') }}"> List Tournaments </a> </li>
							
						  </ul>
						</li>
					
					<li> 
					--}}	
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#"> New  
						<span class="caret"></span></a>
						<ul class="dropdown-menu">
						{{--
							<li> <a href="{{ route('team.player.new')}}"> Player </a> </li>
							<li> <a href="{{ route('member.new')}}"> Member </a> </li>
						--}}
							<li> <a href="{{ route('product.new')}}"> New Product </a> </li>
						{{--
							<li> <a href="{{route('resource.new')}}"> New Resource </a> </li>
							<li> <a href="{{ route('team.new') }}"> New Team </a> </li>
							
							<li> <a href="{{ route('tournament.game.upload') }}"> Upload Game </a> </li>
							<li> <a href="{{ route('gameresults.games') }}"> Game Results </a> </li>
		<!-- 					<li> <a href="{{ route('ad.purchase') }}"> Purchase Ad </a> </li> -->
							<li> <a href="{{ route('player.import') }}"> Import Players </a> </li>
					--}}
						</ul>
					</li>
					{{--
					<li> 	
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#"> Stats/Info
						<span class="caret"></span></a>
						<ul class="dropdown-menu multi-level" role="menu">
							<li> <a href="{{ route('list.scoreboards') }}"> Scoreboards </a> </li>	
							<li> <a href="{{ route('games.live') }}"> Live Games </a> </li>	
							<li> <a href="{{ route('stats_info.mygames',[]) }}"> My Games </a> </li>
							<li> <a href="{{ route('stats_info.mytournaments',[]) }}"> My Tournaments </a> </li>
							<li> <a href="{{ route('stats_info.myteams',[]) }}"> My Teams </a> </li>
							
						  </ul>
						</li>
					--}}
					
					
					<li> 	
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#"> Hi <span id="topNavName"> 
							@if ($user) {{$user->name }} @endif  
							</span>
						<span class="caret"></span></a>
						<ul class="dropdown-menu multi-level" role="menu">
						{{--
									<li> <a href="{{route('profile')}}"> Profile </a></li>
									<li> <a href="{{route('auth.change.pass')}}"> Change Password </a></li>
						--}}
							<li> <a href="{{route('cart.show')}}"> Cart </a> </li>
						{{--
								<li> <a href="{{ route('messaging.create') }}"> Messages </a> </li>
								<li> <a href="{{route('invite')}}"> Invite </a> </li>
								<li> <a href="{{route('org.settings')}}"> Organization </a> </li>
								<li> <a href="{{ route('member.invite') }}"> Invite Friends & Family </a> </li>
							<li> <a href="{{ route('orders') }}"> My Orders </a> </li>
							
								<li> <a href="{{ route('sales') }}"> Sales </a> </li>
								<li> <a href="{{ route('payouts') }}"> Payouts </a> </li>
								
							
							<li> <a href="{{route('howto')}}"> Help </a> </li>
							--}}
							<li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                          	</li>
						  </ul>
						</li>
							
					</ul>
		</div>
	</div>
	</nav>
	
<script>
/*
$(document).ready(function(){
  $('.dropdown-submenu a').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});
*/
</script>

	
	