@foreach($cart->items as $item)
<hr>
	<div class='row'>
		<div class='col-sm-3'>
			{{ $item->Product->name}}
		</div>
		<div class='col-sm-3'>
				{{ $item->description }}
		</div>
		<div class='col-sm-1'>
			{{$item->qty}}
		</div>
		<div class='col-sm-1'>
			${{$item->price}}
		</div>
		<div class='col-sm-1'>
			${{$item->total_price}}
		</div>
		<div class = 'col-sm-1'></div>
		
		<div class = 'col-sm-1'>
		<a href="{{route('cart.remove.item',[
				'id'=>$item->id
		])}}">
			<button class="btn btn-danger" type="button"> - </button>
		</a>
		</div>
	</div>
@endforeach
<hr>
<div class='row'>
	<div class='col-sm-3'>
		Total
	</div>
	<div class='col-sm-5'></div>
	<div class='col-sm-2'> ${{number_format($cart->total,2)}}</div>
</div>

