@extends(config('shoppingcart.layouts.default'))

@section('content')
<br><br>
<h2> Shopping Cart Local</h2>
<hr>
@if ($cart && $cart->items->count() > 0)
	<div class='row'>
		<div class='col-sm-3'> Product </div>
		<div class='col-sm-3'> Description </div>
		<div class='col-sm-1'> Qty </div>
		<div class='col-sm-1'> Price </div>
		<div class='col-sm-1'> Total </div>
	</div>
	@include ('cart.cartContents')
<hr>
<div class='row'>
	<div class='col-sm-3'>
		Total
	</div>
	<div class='col-sm-5'></div>
	<div class='col-sm-2'> ${{number_format($cart->total,2)}}</div>
</div>
<br>
<div class="text-center">
<a href="{{route('cart.checkout')}}">
<button type="button" class="btn btn-info"> Checkout </button>
</a>
<a  href="{{route('cart.empty') }}"> <button type="button" class="btn btn-danger">Empty Cart</button></a>
</div>
@else
	Empty cart
@endif

@if (config('teamplay1.system.debug'))
<br><br>
<pre>
<?php 
if ($cart) {
	$cart->load('items.metas');
	print_r($cart->toArray());
	foreach ($cart->items as $item) {
		if ($item->ForMember)
			echo "For is: " . $item->ForMember->id . "\n";
		echo "Product type: " .$item->product_type_name ."\n";
	}
}
?>
</pre>

@endif


@endsection