
@php
	if (!isset($prefix))
		$prefix="";
@endphp

<div class="row"> 
	<div class="form-group col-md-4">
		<label class="form-label">
			First Name
		</label>
			<input class="form-control input-sm" name="{{$prefix}}first_name" type="text">
	</div>
	
	<div class="form-group col-md-4">
		<label class="form-label">
			Last Name
		</label>
			<input class="form-control input-sm" name="{{$prefix}}last_name" type="text">
	</div>
	
</div>

<div class="row"> 
	<div class="form-group col-md-8">
		<label class="form-label"> Address </label>
		<input class="form-control input-sm" name="{{$prefix}}address_line1">
	</div>
</div>

<div class="row"> 
	<div class="form-group col-md-8">
		<label class="form-label"> Address (line 2) </label>
		<input class="form-control input-sm" name="{{$prefix}}address_line2">
	</div>
</div>

<div class="row"> 
	<div class="form-group col-md-5">
		<label class="form-label"> City </label>
		<input class="form-control input-sm" name="{{$prefix}}city">
	</div>
	<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
    	<label class="form-label" for="{{$prefix}}state" >State</label>

        	<div>
            	@include('owens.page.form.select_state',['name'=>$prefix.'state','old_state'=>old($prefix.'state'),'class'=>'form-control'])
               	@if ($errors->has($prefix.'state'))
                	<span class="help-block">
                		<strong>{{ $errors->first($prefix.'state') }}</strong>
                 	</span>
                @endif
      		</div>
        </div>		
        {{--				
		<div class="form-group col-md-2">
			<label class="form-label"> Country </label>
			<select class="form-control input-sm" name="{{$prefix}}country">
				<option> Option 1 </option>
			</select>
		</div>
		--}}

	<div class="form-group col-md-3">
		<label class="form-label"> Zip/Postal-code </label>
		<input class="form-control input-sm" name="{{$prefix}}postal_code">
	</div>
</div>
	
<div class="row"> 
	<div class="form-group col-md-4">
		<label class="form-label"> Phone </label>
		<input class="form-control input-sm" name="{{$prefix}}phone">
	</div>

	<div class="form-group col-md-4">
		<label class="form-label"> Fax </label>
		<input class="form-control input-sm" name="{{$prefix}}fax">
	</div>
</div>

