@extends(config('shoppingcart.layouts.default'))

@section('content')

<h2> Checkout </h2>
@if (!Auth::user()) 
Please enter your Name and Address then click "Continue" below before selecting Shipping and Payment Method. 
If you already have an account please <a href="{{route('login') }}"> sign in </a>
@endif

@include ('cart.cartContents')
<form method="post">
@include('cart.address_form')

<div class="row">
<div class="col-sm-4">
	<label> Coupon Code </label>
	<input class="form-control" type="text" name="coupon_code">
</div>
</div>
<div class="row">
	<div class="text-center">
		<button class="btn btn-info"> Pay </button>
		{{--
		<a href="{{ route('addmoney.paycartwithpaypal') }}"> <button type="button" class="btn btn-info"> Use Paypal </button></a>
		--}}
		{{--
		<a href="{{ route('payment.stripe') }}"> <button type="button" class="btn btn-info"> Pay with Credit/Debit </button></a>
		--}}
	</div>
</div>
     {{ csrf_field() }}

</form>
@endsection