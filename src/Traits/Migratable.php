<?php

namespace Owens\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

trait Migratable {

	protected function getDeclaration($column) {
		$result['name'] = $column->Field;
		$result['extra'] = $column->Extra;
		$exp = explode(' ',$column->Type);
		$result['type'] = $exp[0];
		if (preg_match('#\(([^)]+)\)#', $exp[0], $match)) {
			$result['size'] = $match[1];
			$result['type'] = strstr($exp[0], '(', true);
		}
		if (isset($exp[1]) && $exp[1] == "unsigned")
			$result['unsigned'] = true;
		else
			$result['unsigned'] = false;
		$result['nullable'] = ($column->Null == "YES")?true:false;
		if ($column->Default != null)
			$result['default'] = $column->Default;
		return $result;
					
	}
	
	// @TODO move this into Migratable
	// @TODO make it a bit more solid for getting tablenames, etc..  Make some functions out of it.
	
	function createManyToMany($table1, $table2, $callback = false, $tablename=false, $id1 = false, $id2 = false) {
		$tables = [str_singular($table1),str_singular($table2)];
		
		if (!$tablename) {
			sort($tables);
			$tablename = $tables[0] ."_". $tables[1];
		}
		$this->table = $tablename;
		$this->columns = [
				$tables[0] ."_id",
				$tables[1] ."_id",
				'created_at',
				'updated_at',
		];
		$this->dropAllForeignKeys($tablename);
		$this->prepare();
		Schema::create($tablename, function (Blueprint $table) use($tables, $table1,$table2,$callback) {
			$table->bigInteger($tables[0] ."_id")->unsigned();
			$table->bigInteger($tables[1] ."_id")->unsigned();
			$table->timestamps();
			$this->addExtraColumns($table);
			
			if ($callback)
				$this->callback($table);
			$table->foreign($tables[0] ."_id")->references('id')->on(str_plural($tables[0]))->onDelete('cascade')->onUpdate('cascade');
			$table->foreign($tables[1] ."_id")->references('id')->on(str_plural($tables[1]))->onDelete('cascade')->onUpdate('cascade');
				
		});
		$this->populate_old_data();
	}
	
	
	// @TODO finish this for more types
	// @TODO test all types..
	protected function createColumn($table, $declare) {
		switch ($declare['type']) {
			case 'float':
				$column = $table->float($declare['name']);
				break;
			case 'double';
				$column = $table->double($declare['name']);
				break;
			case 'bigint';
				$column = $table->bigInteger($declare['name']);
				break;
			case 'integer';
				$column = $table->integer($declare['name']);
				break;
			case 'varchar':
				$column = $table->char($declare['name'],$declare['size']);
				break;
			case 'char':
				$column = $table->char($declare['name'],$declare['size']);
				break;
			case 'timestamp':
				$column = $table->timestamp($declare['name']);
				break;
			case 'datetime':
				$column = $table->datetime($declare['name']);
				break;
			case 'boolean':
				$column = $table->boolean($declare['name']);
				break;
			case 'blob':
				$column = $table->binary($declare['name']);
				break;
			case 'date':
				$column = $table->date($declare['name']);
				break;
			case 'dateTime':
				$column = $table->dateTime($declare['name']);
				break;
			case 'longtext':
				$column = $table->longText($declare['name']);
				break;
			case 'mediuminteger':
				$column = $table->mediumInteger($declare['name']);
				break;
			case 'mediumText':
				$column = $table->mediumText($declare['name']);
				break;
			case 'smallinteger':
				$column = $table->smallInteger($declare['name']);
				break;
			case 'tinyinteger':
				$column = $table->tinyInteger($declare['name']);
				break;
			case 'text':
				$column = $table->text($declare['name']);
				break;
			case 'time':
				$column = $table->time($declare['name']);
				break;
				
			default:
				echo "Unknowntype: " . $declare['type'] . "\n";
				print_r($declare);
				die();
				break;
		}
		if ($declare['unsigned'])
			$column->unsigned();
			if ($declare['nullable'])
				$column->nullable();
				if (isset($declare['default']))
					$column->default($declare['default']);
					return $column;
	}
	
	protected function addColumnFromMysql($table, $MySQLcolumn) {
		$declare = $this->getDeclaration($MySQLcolumn);
		$this->createColumn($table,$declare);
	}
	
	protected function addExtraColumns($table) {
		if (!isset($this->renameTo))
			return;
		$columns = DB::select('show columns from ' . $this->renameTo);
		foreach ($columns as $value) {
			if (isset($this->Columns) && !in_array($value->Field,$this->Columns)) {
				$this->addColumnFromMysql($table,$value);
			}
		}
	}
	
	protected function populate_old_data($callback = false) {
		if (!isset($this->renameTo))
			return;
			$rows = DB::table($this->renameTo)->select('*')->get();
			foreach ($rows as $row) {
				$arow = (array) $row;
				if ($callback)
					$arow = $callback($arow);
					DB::table($this->table)->insert($arow);
			}
			$this->populate_meta();
	}
	
	
	protected function getBackupTableName() {
		return $this->table ."_bak";
	}
	
	protected function prepare() {
		if (Schema::hasTable($this->table)) {
			$this->dropAllForeignKeys($this->table);
			$this->renameTo = $this->getBackupTableName();
			Schema::rename($this->table, $this->renameTo);
			$this->prepareMeta();
		}		
	}
	
	protected function rollback(){
		if (Schema::hasTable($this->getBackupTableName()))
			Schema::rename($this->getBackupTableName(),$this->table);
		$this->rollbackMeta();
	}
	
	protected function populate_meta() {
		if (Schema::hasTable($this->getMetableName() ."_bak")) {
			//			Schema::rename($this->getMetableName() ."_bak",$this->getMetableName());
			echo "Doing raw meta\n";
			DB::insert('insert into ' .$this->getMetableName(). ' select * from ' .$this->getBackupMetableName());
		}
	}
	
	protected function listTableForeignKeys($table)
	{
		$conn = Schema::getConnection()->getDoctrineSchemaManager();
		return array_map(function($key) {
			return $key->getName();
		}, $conn->listTableForeignKeys($table));
	}
	
	protected function dropAllForeignKeys($table) {
		$fkeys = $this->listTableForeignKeys($table);
		if (!empty($fkeys)) {
			Schema::table($table,function($t) use($fkeys) {
				foreach ($fkeys as $fkey)
					$t->dropForeign($fkey);
			});
		}
	}
	
	protected function getBackupMetableName() {
		return $this->getMetableName(). "_bak";
	}
	
	protected function getMetableName() {
		return $this->table . "_meta";
	}
	
	protected function getParentTableId() {
		return str_singular($this->table) . "_id";
	}
	
	protected function prepareMeta() {
		if (Schema::hasTable($this->getMetableName())) {
			$this->dropAllForeignKeys($this->getMetableName());
			Schema::rename($this->getMetableName(), $this->getBackupMetableName());
		}
	}
	
	protected function rollbackMeta() {
		if (Schema::hasTable($this->getBackupMetableName())) {
			Schema::rename($this->getBackupMetableName(),$this->getMetableName());
		}
		
	}
	
	protected function makeMetable() {
		Schema::create($this->getMetableName(), function (Blueprint $table) {
			$table->bigIncrements('id')->unsigned();
			$table->bigInteger($this->getParentTableId())->unsigned();
			$table->char('key',45);
			$table->char('value')->nullable();
			$table->char('type',45)->nullable();
			$table->timestamps();
			// Keys
			$table->foreign($this->getParentTableId())->references('id')->on($this->table)->onDelete('cascade');
		});
	}
	
	protected function dropMetable() {
		Schema::dropIfExists($this->getMetableName());
	}
	
	function getFkeyInfo() {
        $columns = DB::table('information_schema.KEY_COLUMN_USAGE')
            ->join('information_schema.REFERENTIAL_CONSTRAINTS',function($join) {
                $join->on('KEY_COLUMN_USAGE.CONSTRAINT_NAME','=','REFERENTIAL_CONSTRAINTS.CONSTRAINT_NAME')
                ->on('KEY_COLUMN_USAGE.CONSTRAINT_SCHEMA','=','REFERENTIAL_CONSTRAINTS.CONSTRAINT_SCHEMA');
        })->where('KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME',$this->getBackupTableName())
            ->where('KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME','id')
            ->where('KEY_COLUMN_USAGE.CONSTRAINT_SCHEMA',env('DB_DATABASE'))
            ->get();
        return $columns;

    }
    function restoreForeignKeys() {
        $columns = $this->getFkeyInfo();
        foreach($columns as $column) {
            Schema::table($column->TABLE_NAME,function(Blueprint $table) use($column) {
                $table->dropForeign($column->CONSTRAINT_NAME);
				$fkey = $table->foreign($column->COLUMN_NAME)->references($column->REFERENCED_COLUMN_NAME)->on($this->table);
				if ($column->UPDATE_RULE)
					$fkey->onUpdate($column->UPDATE_RULE);
				if ($column->UPDATE_RULE)
					$fkey->onDelete($column->UPDATE_RULE);
            });
        }
    }

    function getBakTables() {
        $bakTables = DB::table('information_schema.TABLES')->select("*")
                ->where('TABLE_NAME','like','%_bak')
                ->where('TABLE_SCHEMA','=',env('DB_DATABASE'))
                ->get();
        return $bakTables;
    }

    function getTableNameFromBak($bakTableName) {
        $len = strlen($bakTableName);
        return substr($bakTableName,0,$len-4);
    }


	
}