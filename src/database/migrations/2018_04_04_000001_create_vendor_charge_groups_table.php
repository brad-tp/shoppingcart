<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorChargeGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_charge_groups', function (Blueprint $table) {
        	$table->bigIncrements('id')->unsigned();
        	$table->bigInteger('invoice_id')->unsigned()->nullable()->default(null);
        	$table->char('transfer_group',60)->default(null)->nullable();
        	$table->bigInteger('stripe_transaction_id')->unsigned()->nullable();
        	$table->char('source_transaction',60)->nullable();
        	$table->char('transfer_type',20)->nullable();
        	$table->char('cc_processor',20)->nullable();
        	$table->timestamps();
        	
   //     	$table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('set null');
   //     	$table->foreign('stripe_transaction_id')->references('id')->on('stripe_transactions')->onDelete('set null');
        	$table->index('transfer_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_charge_groups');
    }
}
