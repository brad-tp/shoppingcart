<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class CreateAddressTable extends Migration
{
	use Migratable;
	
	protected $table = "addresses";
	protected $Columns = ['id','owner_id','first_name','last_name','address_line1','address_line2','city',
			'state','postal_code','country',
			'created_at','updated_at',
	];
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->prepare();
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('owner_id')->unsigned();
            $table->string('first_name',100);
            $table->string('last_name',100);
            $table->string('address_line1');
            $table->string('address_line2')->nullable();
            $table->string('city');
            $table->string('state',5);
            $table->string('postal_code');
            $table->string('country')->nullable();
            $table->timestamps();
            $this->addExtraColumns($table);
            $table->index('owner_id');
        });
        $this->populate_old_data();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
        $this->rollback();
    }
}
