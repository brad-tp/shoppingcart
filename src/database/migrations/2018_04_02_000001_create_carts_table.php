<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class CreateCartsTable extends Migration
{
	use Migratable;
	
	protected $table = 'carts';
	
	protected $Columns = ['id','user_id','key','num_items','total','created_at','updated_at'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->prepare();
        Schema::create('carts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('key',60)->nullable();
            $table->integer('num_items')->default(0);
            $table->double('total')->default(0);
            $table->timestamps();
            $this->addExtraColumns($table);
            
        });
        $this->populate_old_data();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
        $this->rollback();
    }
}
