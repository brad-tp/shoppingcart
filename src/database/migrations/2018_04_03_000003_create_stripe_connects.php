<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeConnects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_connects', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('seller_id')->nullable()->unsigned();
            $table->char('access_token',100);
            $table->char('refresh_token',100);
            $table->char('stripe_publishable_key',100);
            $table->char('stripe_user_id',100);
            $table->char('scope',45);
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
            
            // add Keys
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_infos');
    }
}
