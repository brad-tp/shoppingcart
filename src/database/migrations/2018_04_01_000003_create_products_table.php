<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class CreateProductsTable extends Migration
{
	use Migratable;
	
	protected $table = 'products';
	
	protected $Columns = [
		'id','product_type_id','name','description','price','currency',
			'seller_id','created_by_id','created_at','updated_at',
	];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->prepare();
        Schema::create('products', function (Blueprint $table) {
        	// Basic fields needed for bare minimum product
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('product_type_id')->nullable();
            $table->string('name',100);
            $table->longText('description')->nullable();					// Need to change this for larger descriptions
            $table->double('price');
            $table->string('currency',6)->nullable();
            $table->bigInteger('seller_id')->unsigned()->nullable();
            $table->bigInteger('created_by_id')->unsigned()->nullable();
            $table->timestamps();
            
            // Addition fields for TeamPlay 1
            /*
            $table->char('sport',45)->nullable();
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
            $table->timestamp('signup_expires')->nullable();
            */
            $this->addExtraColumns($table);
        });
        $this->populate_old_data();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        $this->rollback();
    }
}
