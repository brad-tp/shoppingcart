<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingStripeConnects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_stripe_connects', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->char('access_token',100);
            $table->char('refresh_token',100);
            $table->char('stripe_publishable_key',100);
            $table->char('stripe_user_id',100);
            $table->char('scope',45);
            $table->timestamp('accepted_at')->nullable();
            $table->char("key",60);
            $table->timestamps();
            $table->bigInteger('seller_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_stripe_connects');
    }
}
