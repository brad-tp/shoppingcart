<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class CreateInvoiceItemsTable extends Migration
{
	use Migratable;
	
	protected $table = "invoice_items";
	protected $Columns = [
		'id','invoice_id','product_id','buyer_id','seller_id','product_name','description','qty','price','total','product_type',
		'created_at','updated_at'
	];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->prepare();
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('invoice_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('buyer_id')->unsigned()->nullable();
            $table->bigInteger('seller_id')->unsigned();
            $table->string('product_name')->nullable();
            $table->string('description')->nullable();
            $table->integer('qty');
            $table->double('price');
            $table->double('total');
            $table->char('product_type',45)->nullable();
            
            $table->timestamps();
            $this->addExtraColumns($table);
            
            
            // Add keys
        });
        $this->makeMetable();	
        $this->populate_old_data(function ($row) {
        //	$row['buyer_id'] =$row['user_id'];
        	return $row;
        });
        		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	$this->dropMetable();
        Schema::dropIfExists('invoice_items');
        $this->rollback();
    }
}
