<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorChargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_charges', function (Blueprint $table) {
        	$table->bigIncrements('id')->unsigend();
        	$table->char('stripe_user_id',100)->nullable();
        	$table->double('amount')->nullable();
        	$table->char('currency',5);
        	$table->bigInteger('vendor_charge_group_id')->unsigned();
        	$table->double('transfer_amount')->nullable();
        	$table->double('fee')->nullable();
        	$table->char('status',45)->nullable();
        	$table->char('stripe_id',45)->nullable();
        	$table->char('balance_transaction',45)->nullable();
        	$table->bigInteger('invoice_id')->unsigned()->nullable();
        	$table->char('description',100)->nullable();
        	$table->bigInteger('seller_id')->unsigned();
        	$table->char('transfer_type',20)->nullable();
        	$table->char('cc_processor',20)->nullable();
        	$table->char('source_transaction',60)->nullable();        	
        	$table->double('sale_amount')->nullable();
        	
        	$table->timestamps();
        	
   //     	$table->foreign('vendor_charge_group_id')->references('id')->on('vendor_charge_groups');
   //     	$table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_charges');
    }
}
