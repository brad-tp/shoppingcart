<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class CreateInvoicesTable extends Migration
{
	use Migratable;
	
	protected $table = 'invoices';
	
	protected $Columns = [
			'id','buyer_id','first_name','last_name','address','address_line2',
			'city','state','zip','country','currency','subtotal','total','processing','shipping','fee','tax',
			'coupon_discount','coupon_code','payment_type','payment_id',
			'billing_address_id','shipping_address_id',
			'created_at','updated_at',
	];
	
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->prepare();
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('buyer_id')->unsigned();
            $table->char('first_name')->nullable();
            $table->char('last_name')->nullable();
            $table->char('address')->nullable();
            $table->char('address_line2')->nullable();
            $table->char('city')->nullable();
            $table->char('state')->nullable();
            $table->char('zip',15)->nullable();
            $table->char('country')->nullable();
            $table->char('currency',5)->nullable();
            $table->double('subtotal')->nullable();
            $table->double('total')->nullable();
            $table->double('processing')->nullable();
            $table->double('shipping')->nullable();
            $table->double('fee')->nullable()->default(0);
            $table->double('tax')->nullable()->default(0);
            
            $table->double('coupon_discount')->nullable();
            $table->char('coupon_code')->nullable();
            
            $table->char('payment_type')->nullable();
            $table->char('payment_id')->nullable();
  //          $table->bigInteger('user_id')->unsigned()->nullable();		// Don't thiink this will be needed
            $table->bigInteger('billing_address_id')->unsigned()->nullable();
            $table->bigInteger('shipping_address_id')->unsigned()->nullable();
            
            $table->timestamps();
            $this->addExtraColumns($table);
            
  //          $table->bigInteger('org_id')->nullable();		// Purchased through -- get rid of this...
            
            // Create Keys
            $table->index('billing_address_id');
            $table->index('shipping_address_id');
            $table->index('buyer_id');
        });
        
        	$this->populate_old_data(function ($row) {
        	//	$row['buyer_id'] = $row['user_id'];
        		return $row;
        	});
        		
        		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        $this->rollback();
        
    }
}
