<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class CreateCartItemsTable extends Migration
{
	use Migratable;
	
	protected $table="cart_items";
	protected $Columns = [
			'id','cart_id','product_id','qty','pay_id','description','product_name','created_at','updated_at',
	];
	protected $fkeys;
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->prepare();
        Schema::create('cart_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->integer('qty')->unsigned()->default(0);
            $table->string('pay_id')->nullable();
            $table->string('description')->nullable();
            $table->string('product_name')->nullable();
            $table->timestamps();
            $this->addExtraColumns($table);
            $table->foreign('cart_id')->references('id')->on('carts')->onDelete('cascade');
        });
        $this->makeMetable();	
        $this->populate_old_data();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	$this->dropMetable();
        Schema::dropIfExists('cart_items');
        $this->rollback();
    }
}
