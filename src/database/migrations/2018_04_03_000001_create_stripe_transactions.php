<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class CreateStripeTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_transactions', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->char('stripe_id',100);
            $table->char('object',45);
            $table->double('amount');
            $table->double('amount_refunded')->nullable();
            $table->char('currency',5);
            $table->char('balance_transaction',100);
            $table->bigInteger('invoice_id')->unsigned()->nullable();
            $table->char('transfer_group',45);
            $table->bigInteger('stripe_transfer_group_id')->unsigned();
            
            $table->timestamps();
            
            $table->foreign('invoice_id')->references('id')->on('invoices');
 //           $table->foreign('stripe_transfer_group_id')->references('id')->on('stripe_transfer_groups')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_transactions');
    }
}
