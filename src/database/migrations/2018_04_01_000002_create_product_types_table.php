<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	if (!Schema::hasTable('product_types')) {
	        Schema::create('product_types', function (Blueprint $table) {
	            $table->bigIncrements('id')->unsigned();
	            $table->string('name',45);
	            $table->integer('product_style_id');
	            // $table->timestamps();
	        });
    	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_types');
    }
}
