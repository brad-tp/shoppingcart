

$('document').ready(function() {
	
	$('[data-strip-card=true]').each(function(index,element) {
		console.log("Loaded strip card",element);
		var key = $(element).data('key');
		console.log("key: ",key);
		var form = element.closest('form');
		console.log("form is", form);
		console.log("Form is: ",$(form).attr('id'));

		var stripe = Stripe(key);
		var elements = stripe.elements();

		var card = elements.create('card');

		console.log("ID is: ", $(element).attr('id'));
		// Add an instance of the card UI component into the `card-element` <div>
		card.mount(element);

		function stripeTokenHandler(token) {
			  // Insert the token ID into the form so it gets submitted to the server
//			  var form = document.getElementById('subscription-form');
			  var hiddenInput = document.createElement('input');
			  hiddenInput.setAttribute('type', 'hidden');
			  hiddenInput.setAttribute('name', 'stripeToken');
			  hiddenInput.setAttribute('value', token.id);
			  form.appendChild(hiddenInput);

			  // Submit the form
			  form.submit();
			}

			function createToken() {
			  stripe.createToken(card).then(function(result) {
			    if (result.error) {
			      // Inform the user if there was an error
			      var errorElement = document.getElementById('card-errors');
			      errorElement.textContent = result.error.message;
			    } else {
			      // Send the token to your server
			      stripeTokenHandler(result.token);
			    }
			  });
			};

			// Create a token when the form is submitted.
	//		var form = document.getElementById('subscription-form');
			
			form.addEventListener('submit', function(e) {
			  e.preventDefault();
			  var use_card = $('[name=use_card]');
			  console.log("use card: ",use_card.val());
			  console.log("usa card checked: ", use_card.prop('checked'));
			  if (use_card.length && use_card.prop('checked'))
				  form.submit();
			  else
				  createToken();
			});



	});
	
//	Stripe.setPublishableKey('pk_test_laY4IxLiWBMfbv3TjTMNEi2w');
	/*
	$('#subscription-form button').on('click',function() {
		console.log("CC Button clicked");
		var form=('#subscription-form');
		var submit = form.find('button');
		var submitInitialText = submit.text();
		submit.attr('disabled','disabled')
		
	});
	*/
});