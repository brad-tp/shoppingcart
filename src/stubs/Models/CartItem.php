<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

class CartItem extends Owens\ShoppingCart\Models\CartItem {
	
	public function getPriceAttribute() {
		return number_format($this->Product->price,2);
	}
	
	public function getTotalPriceAttribute() {
		return number_format($this->Product->price * $this->qty,2);
	}
}