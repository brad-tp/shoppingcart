<?php

namespace App\Http\Controllers\Stripe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Stripe\Event as StripeEvent;
use App\Models\StripeTransferGroup;
use Stripe\Transfer as StripeTransfer;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class WebHooksController extends \Owens\Stripe\Controllers\Stripe\WebhooksController
{
    
    /**
     * Handles charge.refuned
     * 
     * @param unknown $payload
     */
    public function handleChargeRefunded($payload) {
    	Log::info("Handle Charge Refunded: " . print_r($payload,true));
    	return response("Webhook handled",200);
    }
    
    function doReversalFee($payload) {
    	Log::info("Do Revcersal Fee");
    	$reverse_amount = $payload->data->object->amount_reversed/.9;
    	$reversal_fee = $reverse_amount * .10;
    	$metaData = [];
    	
    	$options = [
    			"amount" => $reversal_fee,
    			"currency" => $payload->data->object->currency,
    			"destination" => StripeFacade::getAccount(),
    			"metadata" => $metaData,
    	];
    	//print_r($options);
    	try {
    		$transfer = StripeTransfer::create($options,['stripe_account'=> $payload->data->object->destination]);
    		echo "Transfer\n";
    		print_r($transfer);
    		/*
    		$vendor->status = 'success';
    		$vendor->stripe_id = $transfer->id;
    		$vendor->balance_transaction = $transfer->balance_transaction;
    		*/
    	} catch (\Exception $e) {
    		//$vendor->status = 'failed';
    		Log::info( "Transfer failed " . $e->getMessage());
    	}
    	
    }
    
    function handleTransferReversed($payload) {
    	Log::info("Handle Transfer Reversed: " . print_r($payload,true));
    	Log::info("Amount reversed: " . $payload->data->object->amount_reversed);
    	Log::info("Order group: " . $payload->data->object->transfer_group);
    	Log::info("Account: " . $payload->data->object->destination);
    	$transferGroup = StripeTransferGroup::where('transfer_group',$payload->data->object->transfer_group)->first();
    	if ($transferGroup)
    		Log::info("Transfer Group: " . print_r($transferGroup->toArray(),true));
    	else 
    		Log::info("No transfer group");
    	$reverse_amount = $payload->data->object->amount_reversed/.9;
    	$this->doReversalFee($payload);
    	
    	
    }
    
}
