<?php

namespace App\Http\Controllers\Stripe;

use Illuminate\Http\Request;
use Owens\ShoppingCart\Facade\ShoppingCart;
use Owens\Stripe\Stripe;
use Stripe\Account;
use Stripe\Token as StripeToken;
use Owens\Stripe\Exceptions\StripeException;
use Owens\Stripe\StripeConnectCharge;
use Owens\Stripe\Facade\Stripe as StripeFacade;


class StripeChargeController extends \Owens\Stripe\Controllers\Stripe\StripeChargeController
{
	
	protected $billing_address = [
			'first_name' => 'Brad',
			'last_name' => 'Owens',
			'address_line1' => '16120 Nall Ave',
			'city' => 'Stilwell',
			'state' => 'KS',
			'postal_code' => '66085',
	];
	protected $shipping_address = [
			'first_name' => 'Brad',
			'last_name' => 'Shipping',
			'address_line1' => '16120 Nall Ave',
			'city' => 'Stilwell',
			'state' => 'KS',
			'postal_code' => '66085',
			
	];
	
	function getShippingAddress($owner = null) {
		return $this->shipping_address;
	}
	
	function getBillingAddress($owner = null) {
		return $this->billing_address;
	}
	
	function prepareFormRequest(Request $request) {
		return true;
	}
	
	function getMetas() {
		return ['testing' => "Test Meta"];
	}
	
	
	function handleStripeChargeFailed($message) {
		return $this->flashBackError("Error! Please Try again. " . $message());
	}
	
	function index() {
		$cart = ShoppingCart::cart();
		return view('owens.stripe.index',
				[
					'cart'=>$cart,
					'api_key' => StripeFacade::getKey(),	
				]);
	}
	
	function post(Request $request) {
		$this->prepareFormRequest($request);
		$token = $request->input ( 'stripeToken',null );
		echo "<PRE>";
//		print_r($request->all());
		$card = $request->input('card',false);
		$connectCharge = new StripeConnectCharge($this,\Auth::user(),$token,$card);
		try {
			$result = $connectCharge->Charge();
			$invoice = $connectCharge->getInvoice();
			$msg = "Payment Successful";
			return $this->flashRouteInfo($msg,'invoice.show',['id'=>$invoice->id]);
			
		} catch (StripeException $e) {
	//		throw $e;
			$p = $e->getPrevious();
			if ($p)
				return $this->flashBackError($e->getLabel() . " : " . $e->getMessage() . " : " . $p->getMessage());
			else
				return $this->flashBackError($e->getLabel() . " : " . $e->getMessage() . " : " ."no previous");
		} 
		echo "Exiting\n";
	}
	
	function fetch(Request $request) {
		echo "<PRE>";
		echo "Strip Charge Fetch: " . $request->stripeToken;
		//	die();
		
		$token=$request->input('stripeToken');
		//		\Stripe\Stripe::setApiKey ( 'sk_test_zAkXbJiRhicCPlbH34DGidpa' );
		try {
			$result = \Stripe\Charge::retrieve($token);
			echo "Fetched\n";
			print_r($result);
		} catch (\Exception $e) {
			echo "Failed to fetch: " . $e->getMessage();
		}
	}

	
		
	function fetchAccount($acct=false) {
		try {
			if ($acct)
				$account = Account::retrieve($acct);
			else 
				$account = Account::retrieve();
		} catch (\Exception $e) {
			return false;
		}
		return $account;
	}
		 
}
