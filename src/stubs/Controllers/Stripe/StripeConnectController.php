<?php

namespace App\Http\Controllers\Stripe;

use Illuminate\Http\Request;
use Owens\Stripe\Stripe;
use Stripe\Account as StripeAccount;
use Owens\Stripe\Facade\Stripe as StripeFacade;

class StripeConnectController extends \Owens\Stripe\Controllers\Stripe\StripeConnectController
{
	protected $user;
	
	function __construct(Stripe $stripe) {
		// Workaround for no access to sessions.
		$this->middleware(function ($request, $next) {
			$this->user= \Auth::user();
			$this->stripeConnectManager = new \Owens\Stripe\StripeConnectManager($this->user);
			return $next($request);
		});			
	}
    //
	function canOnboard() {
		return true;
	}
	
	function index() {
		if (!$this->canOnboard()) {
			return $this->handleFail('onboard.not.allowed',"You are not authorized to accept credit cards");
		}
		return view('owens.stripe.connect',[
				'client_id'=>StripeFacade::getClientId(),
		]);
	}
	
	function onboard(Request $request, Stripe $stripe) {
		if (!$this->canOnboard()) {
			return $this->handleFail('onboard.not.allowed',"You are not authorized to accept credit cards");
		}
		if ($request->error) {
			return $this->handleFail('stripe.onboard.error',$request->error_description);
		}
		$token=$request->code;
		if (($info = $this->stripeConnectManager->handleOnboard($token)) === false)
			return $this->handleFail('stripe.onboard.fail',$this->stripeConnectManager->getErrors()[0]);
		if ($info === true)
			return view("owens.stripe.check_email");
		else
			return view("owens.stripe.onboard");
	}
	
	function activeStripe(Request $request, $key) {
		if (!$this->canOnboard())
			return $this->handleFail('onboard.not.allowed');
		if (!$this->stripeConnectManager->acceptPendingConnect($key))
			return $this->handleFail('activate.fail',$this->stripeConnectManager->getErrors()[0]);
		return view("owens.stripe.onboard");
	}
	
	function handleStripeOnboardError($message) {
		session()->flash('flash_info','Onboard error: ' . $message);
		return redirect()->route('stripe.connect');
	}
	
	function handleStripeOnboardFail($message) {
		return view('owens.stripe.onboard_fail',['reason'=>$message]);
	}
	
	function customAccount() {
		echo "<pre>\n";
		echo "Custom account\n";
		$options = [
				'type'=>'custom',
				'country'=>'US',
				'email' => \Auth::user()->email,
		];
		try {
			$acct = StripeAccount::create($options);
		} catch (\Exception $e) {
			echo "Failed to create account: " . $e->getMessage() . "\n";
			die();
		}
		print_r($acct);
		$acct->access_token = "No Token";
		$acct->refresh_token = "No Token";
		$acct->stripe_publishable_key = "No Key";
		$acct->stripe_user_id = $acct->id;
		$acct->scope = "None";
		return $this->setUpAccountActivation($acct);
	}
		
}
