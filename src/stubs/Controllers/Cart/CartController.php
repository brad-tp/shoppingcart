<?php
namespace App\Http\Controllers\Cart;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Owens\ShoppingCart\Facade\ShoppingCart;
use Owens\ShoppingCart\Traits\Controllers\Cartable;

class CartController extends \Owens\ShoppingCart\Controllers\Cart\CartController
{
	
	function localPrepareCartContent($options, $product) {
			return $options;	
	}
	
	function localVerifyCart($options,$product) {
		return true;
	}
}