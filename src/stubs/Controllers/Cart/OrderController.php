<?php

namespace App\Http\Controllers\Cart;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Invoice;

class OrderController extends \Owens\ShoppingCart\Controllers\Cart\OrderController
{
    protected $view = 'orders.index';
        
}
